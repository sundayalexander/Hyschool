<?php
namespace App\Controllers;
/**
*This is an abstract ControllerAdapter class. This class
*implements the Controller interface.
*This class must be extended every controller class. 
*@author Amowe Sunday Alexander
*@version 1.0
*/

//use the required namespace
use App\Util\Properties;
use App\Controllers\Controller;

//include the necessary files here
include_once 'Controller.interface.php';


abstract class ControllerAdapter implements Controller{

    /**
     *This method handles POST request sent by the
     *client.
     * @param Properties $params This is the parameter
     *of the request sent by the client.
     */
	public function post(Properties $params){
		
	}

    /**
     *This method handles GET request sent by the
     *client.
     * @param Properties $params This is the parameter
     *of the request sent by the client.
     */
	public function get(Properties $params){
		
	}
	
	
}

