<?php
namespace App\Controllers;
/**
*This is a Controller Interface. This interface
*define the architectural principle behind the
*Controller adapters.
*@author Amowe Sunday Alexander
*@version 1.0
*/

//user the required namespace
use App\Util\Properties;


interface Controller{

    /**
     *This method handles POST request sent by the
     *client.
     * @param Properties $params This is the parameters
     *of the request sent by the client
     * @return
     */
	public function post(Properties $params);

    /**
     *This method handles GET request sent by the
     *client.
     *@param Properties $params This is the parameter
     *of the request sent by the client.
     *@return
     */
	public function get(Properties $params);
	
	
}

