<?php
namespace App\Util;

//use required namespace
use \InvalidArgumentException;
use App\Util\Iterator;

//include required files
include 'Iterator.class.php';

/**
*This is a property class. This class
*based on the java.util.Properties class, this
*class is used to parse parameter between
*the database class.
*@author Amowe Sunday Alexander
*@version 1.0
*/
final class Properties{
	//Properties goes here
	private $properties; 
	const ASEC = 0;
	const DESC = 1;
	const KEY_MODE = 0; //This is the key mode of the properties class
	const VALUE_MODE = 1; //This is the value mode of the properties clas.

    /**
     * This constructor initialize the
     * properties variable.
     * @param array $properties this is an associative array
     * of properties.
     * @throw InvalidArgumentException this
     * is thrown if the supplied argument is
     * not a String data type or null.
     */
	function __construct(array $properties = NULL){
		if(is_null($properties)){
			$this->properties = array();
		}else if(is_array($properties)){
			$this->properties = $properties;
		}else{
			throw new InvalidArgumentException("The argument provided is not an assoc array type.");	
		}
	}

    /**
     * This method appends the given key and value
     * to this class properties variable.
     * @param  $key this is the key of the
     * associative array.
     * @param $value this is the value of the
     * key set earlier.
     * @throw InvalidArgumentException this
     * is thrown if the supplied key is not
     * a String data type.
     */
	public function put($key, $value){
		if(is_string($key)){
			$this->properties[$key] = $value;
		}else{
			throw new InvalidArgumentException("The key provided is not a string data type.");	
		}
	}
	
	/**
	* This method returns the value of the given 
	* key and value to this class properties variable.
	*@param $key this is the key of the 
	* associative array.
	*@throw InvalidArgumentException this
	* is thrown if the supplied key is not
	* a String data type.
	*/
	public function getProperty($key){
		if(is_string($key)){
			return $this->properties[$key];
		}else{
			throw new InvalidArgumentException("The key provided is not a string data type.");	
		}
	}

    /**
     * This method update the value of the supplied
     * key with the supplied value.
     * @param $key this is the key of value to be updated.
     * @param $value this is the value to be inserted.
     * @throw InvalidArgumentException this
     * is thrown if the supplied key is not
     * a String data type.
     * @return bool
     */
	public function setProperty($key, $value){
		if(is_string($key)){
			if(isset($this->properties[$key])){
				$this->properties[$key] = $value;
				return true;
			}else {
				return false;	
			}
		}else{
			throw new InvalidArgumentException("The key provided is not a string data type.");	
		}
	}
	
	/**
	* This method returns the size of the property array. 
	*/
	public function size(){
		return count($this->properties);
	}
	
	/**
	* This method remove the element with the
	* supplied key from the properties array.
	*@param $key this is the key of the 
	* associative array.
	*@throw InvalidArgumentException this
	* is thrown if the supplied key is not
	* a String data type.
	*@return This is set to true if the element
	* with the supplied key could be removed otherwise
	* false.
	*/
	public function remove($key){
		if(is_string($key)){
			if(isset($this->properties[$key])){
				unset($this->properties[$key]);
				return true;
			}else{
				return false;
			}
		}else{
			throw new InvalidArgumentException("The key provided is not a string data type.");	
		}
	}

    /**
     * This method checks if the supplied key
     * exist in the properties array.
     *
     * @param array $needles this is the list
     * of key to test against.
     * @param int $mode this is the mode of test
     * or search, this could be either key_mode or
     * value_mode.
     * @return This is set to true if the key supplied
     * exist in the properties array otherwise false.
     * @throw InvalidArgumentException this
     * is thrown if the supplied key is not
     * a String data type.
     *
     */
	public function contains(array $needles, $mode = Properties::KEY_MODE){
		if(is_array($needles)){
			//switch between modes
			switch($mode){
				case Properties::KEY_MODE: //test for keys
					if($this->isEmpty()){
						throw new InvalidArgumentException("contains(array,int) expect parameter 1 to be array null given");
					}
					foreach($needles as $key){
						if(!in_array($key, $this->getKeys())){
							return false;
						}
					}
					return true;
				break;
				case Properties::VALUE_MODE: //test for values
					if($this->isEmpty()){
						throw new InvalidArgumentException("contains(array,int) expect parameter 1 to be array null given");
					}
					foreach($needles as $value){
						if(!in_array($value, $this->getValues())){
							return false;
						}
					}
					return true;
				break;
				default:
				throw new InvalidArgumentException("mode can only assume one of two states: 0 or 1");
			}
		}else{
			throw new InvalidArgumentException("The needles provided is not an array data type.");	
		}
	}
	
	/**
	* This method returns array of keys present
	* in the properties array.
	*@return This is set array of keys if the
	* properties array is not empty otherwise
	* NULL.
	*/
	public function getKeys(){
		if(count($this->properties) > 0){
			$keys = array();
			foreach($this->properties as $key => $value){
				$keys[] = $key;
			}
			return $keys;
		}else {
			return NULL;	
		}
	}
	
	/**
	* This method returns array of values present
	* in the properties array.
	*@return This is set to array of values if the
	* properties array is not empty otherwise
	* NULL.
	*/
	public function getValues(){
		if(count($this->properties) > 0){
			$values = array();
			foreach($this->properties as $key => $value){
				$values[] = $value;
			}
			return $values;
		}else {
			return NULL;	
		}
	}
	
	/**
	* This method returns a string of key and value
	* pair of the properties.
	*@return This is set to string of key and value
	* pairs otherwise NULL.
	*/
	public function toString(){
		return serialize($this->properties);
	}
	
	/**
	* This method returns checks if the propreties
	*array is empty or not by which boolean is returned.
	*@return This is set to true if the properties is empty
	* otherwise false.
	*/
	public function isEmpty(){
		if(count($this->properties) > 0){
			return false;
		}else {
			return true;	
		}
	}
	
	/**
	*This method returns Iterator object on this class
	*@return This is set to an iterator object.
	*/
	public function iterator(){
		return new Iterator($this);
	}

    /**
     * This method sorts the propreties based on
     * either key or value mode using either
     * Ascending or descending techniques.
     * @param $order This is the order in which the
     * the method is to sort the properties.
     */
	public function sort($order){
		if($order == Properties::ASEC){
			natsort($this->properties);
		}else if($order == Properties::DESC){
			arsort($this->properties);
		}else{
			throw new InvalidArgumentException("The supplied argument is not a valid argument range: 0-1");	
		}
	}
}

?>