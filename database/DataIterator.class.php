<?php
namespace Database;
/**
*This is an Iterator class. This class
*based on Java Iterator class, this
*class hides the technicality of the 
*data looping from the user by providing
*easy to use methods
*@author Amowe Sunday Alexander
*@version 1.0
*/

//use the required namespace
use App\Util\Properties;

//Import the required classes.


final class DataIterator{
	//properties goes here
	private static $counter;
	private $size, $prop, $valueCounter;
	
	//Constructor method
	public function __construct(Properties $prop){
		$this->prop = $prop;
		$it =$this->prop->iterator();
		$value = $it->nextValue();
		$this->size = count($value);
        $this->valueCounter = array();
        $keys = $this->prop->getKeys();
        if($keys != NULL){
            foreach($keys as $key){
                $this->valueCounter[$key] = 0;
            }
        }


	}
	
	/**
	*This method checks if there is still 
	*element to loop through.
	*@return bool: This is set to true if
	*there is still element to loop through
	*otherwise false.
	*/
	public function hasNext(){
		if(DataIterator::$counter < $this->size){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	*This method returns the key of 
	*the next element in properties object.
	*@param string $column: this is the column
	*name to retrieve
	*@return string: This is set to the next
	*value.
	*/
	public function next($column){
        if($this->valueCounter[$column] < $this->size){
            $values = $this->prop->getProperty($column);
            $value = $values[$this->valueCounter[$column]];
            $this->valueCounter[$column]++;
            DataIterator::$counter = $this->valueCounter[$column];
            return $value;
        }

	}
	
	
}
?>