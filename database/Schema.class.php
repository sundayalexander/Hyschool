<?php
namespace Database;
/**
*This is a schema class. This class
*based on laravel schema class, this
*class hides the technicality of the 
*SQL  queries from the user by providing
*easy to use interface with various methods
*@author Amowe Sunday Alexander
*@version 1.0
*/

//use the required namespace
use Database\Table;
use Database\Connection;
use \MySQLi_Sql_Exception;


//include the necessary files here
include_once 'Table.class.php';


final class Schema extends Connection{
	//Class properties goes here


    /**
     * This method creates database table.
     * @param Table $table This is a table object
     *in which the getStructure method is triggered
     * to get the structure of the table as string.
     * @return This is set to true if the table
     *was successfully created otherwise false
     * @throws MySQLi_Sql_Exception
     */
	public static function createTable(Table $table){
        Schema::connector();
		if(Schema::$conn == NULL){
			throw new MySQLi_Sql_Exception("No database selected.");
		}
		return Schema::$conn->real_query($table->getStructure());
	}

    /**
     * This method creates a new database
     *@param string $db: this is the name of the database
     * to be created.
     *@return this is set to true if the database
     *could be created otherwise false.
     */
    public static function createDB($db){
        $query = Schema::$conn->real_query("CREATE DATABASE IF NOT EXISTS ".$db);
        if($query){
            Schema::$conn->real_query("USE ".$db);
            return true;
        }else {
            return false;
        }
    }

    /**
     * This method deletes a given table.
     *@param string $table This is the
     * name of the table to be dropped.
     *@return This is set to true if the table
     *was successfully dropped otherwise false
     */
    public function deleteTable($table){
        return  Schema::$conn->real_query("DROP TABLE IF EXISTS ".$table);
    }
	
}

