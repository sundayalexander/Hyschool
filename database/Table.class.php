<?php
namespace Database;
/**
*This is a Table class. This class is
*based on java JDBC class, this
*class hides the technicality of the 
*SQL  queries from the user by providing
*easy to use methods
*@author Amowe Sunday Alexander
*@version 1.0
*/
final class Table {
	/* Property goes here */
	private $query; //This is the query string
	private $table; //This is the table name
	private $structure = ""; //This is the table structure query.
	private $engine; //This is the table's storage engine
	
	//constants  for table storage engine
	const ENGINE_MYISAM = "MyISAM";
	const ENGINE_INNODB = "InnoDB";
	const ENGINE_MRG_MYISAM = "MRG_MYISAM";
	const ENGINE_MEMORY = "MEMORY";
	const ENGINE_BLACKHOLE = "BLACKHOLE";
	const ENGINE_CSV = "CSV";
	const ENGINE_ARCHIVE = "ARCHIVE";
	
	//constants for Binary Large Object (BLOB)
	const BLOB_TINY = "TINYBLOB";
	const BLOB_MEDIUM = "MEDIUMBLOB";
	const BLOB_NORMAL = "BLOB";
	const BLOB_LONG = "LONGBLOB";

    /**
     *This constructor initialize the
     * @param This is the table name
     * @param string $engine this is the
     * engine type used by the table.
     */
	public function __construct($name, $engine = Table::ENGINE_MYISAM){
		$this->engine = $engine;
		$this->query = "CREATE TABLE IF NOT EXISTS ".$name."(";
		$this->table = $name;
	}
	
	
	/**
	*This method structure a column with varchar data
	*type and set the name and size as speicified or
	*provided as argument to the method with default
	*value NOT NULL.
	* 
	*@param string $name this the name of the column
	*@param int $size this is the total character size 
	* of the column.
	*/
	public function setString($name, $size = 191){
		$this->structure .= ", ".$name." varchar(".$size.") NOT NULL";
	}

    /**
     *This method structure a column with timestamp data
     *type and CURRENT TIME STAMP as the default value.
     *
     * @param string $name this the name of the column
     * @param bool $not_null this is the not null
     * clause (condition)
     * @param bool $default_value this is the default
     * value condition.
     */
	public function setTimestamp($name, $not_null = true, $default_value = true){
		$string = ", ".$name;
		if($not_null){
			$string .= " TIMESTAMP NOT NULL ";
		}else {
			$string .= " TIMESTAMP NULL ";
		}
		if($default_value){
			$string .= "DEFAULT CURRENT_TIMESTAMP";
		}else{
			$string .= "DEFAULT NULL";
		}
		$this->structure .= $string;
	}
	
	/**
	*This method structure a column with text data type.
	* 
	*@param string $name this the name of the column
	*/
	public function setText($name){
		$this->structure .= ", ".$name." TEXT NOT NULL";
	}

    /**
     *This method structure a column with int data
     *type and set the name, size, unsigned, auto_increment
     * as speicified or provided as argument to the
     * method with default value NOT NULL.
     *
     * @param string $name this the name of the column
     * @param int $size this is the length or size of the column.
     * @param bool $aut_increment
     * @param boolean $unsigned determines if the data is
     * unsinged or not
     * @throws InvalidArgumentException
     */
	public function setInt($name, $size = 10, $aut_increment = true, $unsigned = true){
		$string = ", ".$name." int(";
		if($size < 1 || $size > 255){
			throw new InvalidArgumentException("int size can only be between the range: 1-255");
		}
		$string .= $size.") ";
		//Test if unsigned
		if($unsigned){
			$string .= "UNSIGNED NOT NULL";
		}else {
			$string .= "NOT NULL";
		}
		//Test if auto increment
		if($aut_increment){
			$string .= " AUTO_INCREMENT";
		}
		$this->structure .= $string;
	}

    /**
     *This method set the primary key of the table.
     *
     * @param $column this is the name of the column
     * or field.
     */
	public function primaryKey($column){
		$this->structure .=  ", PRIMARY KEY (".$column.")";
	}

    /**
     *This method set a column as unique.
     *
     * @param $column this is the name of the
     * column or field.
     */
	public function setUnique($column){
		$this->structure .=  ", UNIQUE KEY ".
		$this->table."_".$column."_unique (".$column.")";
	}

    /**
     *This method index a column as full text
     *
     * @param $column this is the name of the column
     */
	public function fullText($column){
		$this->structure .=  ", FULLTEXT ".
		$this->table."_".$column."_fulltext (".$column.")";
	}

    /**
     *This method structure a column with BOOLEAN data
     *type and set the default value NOT NULL.
     *
     * @param $column this is the name of the filed
     * or column.
     *
     */
	public function setBoolean($column){
		$this->structure .=  ", ".$column." BOOLEAN NOT NULL";
	}

    /**
     *This method structure a column with Binary
     * Large Object (BLOB) data type and set
     * the default value NOT NULL.
     *
     * @param int size this indicate the type of blob
     * @param string $type
     * @throws InvalidArgumentException
     * @internal param string $name this the name of the column
     */
	public function setBLOB($column, $type = Table::BLOB_NORMAL){
		//switch between blobs
		switch($type){
			case Table::BLOB_LONG:
				$this->structure .=  ", ".$column." ".Table::BLOB_LONG." NOT NULL";
			break;
			case Table::BLOB_MEDIUM:
				$this->structure .=  ", ".$column." ".Table::BLOB_MEDIUM." NOT NULL";
			break;
			case Table::BLOB_NORMAL:
				$this->structure .=  ", ".$column." ".Table::BLOB_NORMAL." NOT NULL";
			break;
			case Table::BLOB_TINY:
				$this->structure .=  ", ".$column." ".Table::BLOB_TINY." NOT NULL";
			break;
			default: throw new InvalidArgumentException("The supplied type is not a valid type");
			
		}		
	}

    /**
     *This method structure a column with Javascript
     * Object Notation (JSON) data type and set
     * the default value NOT NULL.
     *
     * @param $column this is the name of the field.
     */
	public function setJSON($column){
		$this->structure .=  ", ".$column." JSON NOT NULL";
	}
	
	/**
	*This method returns the table structure as string.
	*/
	public function getStructure(){
		return $this->query.substr($this->structure,1).") ENGINE=".$this->engine;
	}
	
}

?>