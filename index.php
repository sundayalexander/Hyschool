<?php
use App\Controllers\LoginController;
use App\Util\Properties;

define('DIR_APPLICATION', str_replace('\'', '/', realpath(dirname(__FILE__))) . '/');

//File Inclusion goes here
require_once('app/controllers/LoginController.class.php');

ob_start();
session_start();

//Redirect from index page if user is loged in
$controller = new LoginController();
$controller->redirect(new Properties($_SESSION));
$msg = $controller->login(new Properties($_POST));






?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title></title>
<!-- BOOTSTRAP STYLES-->
<link href="res/assets/css/bootstrap.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="res/assets/css/font-awesome.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="res/assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<style type="text/css">
.btn-primary{
border-radius: 0px;
}
a {
color:#ffcc33;
font-weight: bold;

}
a:hover {
color:#ffcc33;


}
</style>


</head>
<body>
<!-- <br>
  <br>
  <br>
  <br> -->
  <br />
<div class="container" style="height:100%">
  <div class="row text-center ">
    <div class="col-md-12"> <br />
      <!-- <img style="width:105px;height:100px;" src="<?php echo WEB_URL; ?>img/upload/img.png" /> --> </div>
  </div>


  <div class="row ">
    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12 col-xs-offset-0" style="border-radius:0px;">
      <div style="margin-bottom:8px;padding-top:2px;width:100%;height:25px;background:#E52740;color:#fff; display:<?php echo $msg; ?>" align="center">Wrong login information</div>
      <div class="panel panel-default" id="loginBox">
        <div class="panel-heading"> <strong> Enter Login Details </strong> </div>
        <div class="panel-body">
          <form onSubmit="return validationForm();" role="form" id="form" method="post">
            <br />
            <div class="form-group input-group"> <span class="input-group-addon"><i class="fa fa-at"  ></i></span>
              <input type="text" name="username" id="username" class="form-control" placeholder="Your Email " Style="height:50px" />
            </div>
            <div class="form-group input-group"> <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>
              <input type="password" name="password" id="password" class="form-control"  placeholder="Your Password" Style="height:50px" />
            </div>
            <div class="form-group input-group"> <span class="input-group-addon"><i class="fa fa-user"  ></i></span>
              <select name="ddlLoginType" id="ddlLoginType" class="form-control" Style="height:50px" >
                <option value="-1">-- Select Type --</option>
                <option value="admin">Admin</option>
                <option value="teacher">Teacher</option>
                <option value="student">Student</option>
                <option value="parents">Parent</option>
                <!-- <option value="accountant">Accountant</option>
				<option value="librarian">Librarian</option> -->
              </select>
            </div>
            <div class="form-group" >
              <label class="checkbox-inline">
              </label>
              <span class="pull-right"> <a href="#" style="color: #31516A">Forgot password ? </a> </span> </div>
            <hr />
            <div align="center">
              <button style="width:100%;" type="submit" id="login" class="btn btn-primary" name="login">Login</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function validationForm(){
	if($("#username").val() == ''){
		alert("Username Required !!!");
		$("#username").focus();
		return false;
	}
	else if($("#password").val() == ''){
		alert("Password Required !!!");
		$("#password").focus();
		return false;
	}
	else if($("#ddlLoginType").val() == '-1'){
		alert("Select Login Type !!!");
		return false;
	}
	else{
		return true;
	}
}
</script>
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="res/assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="res/assets/js/bootstrap.min.js"></script>
</body>
</html>
