<?php
namespace App\Util;


/**
 * Class URL this is configuration file.
 */
class URL{
    //Definition of constant properties
    const VENDOR = './res/vendor/';
    const RES = './res/';
    const ASSETS = './res/assets/';
    const LAYOUT = './res/layout/';
    const CUSTOM = './res/custom/';
    const DATABASE = './database/';
    const STORAGE = './storage/';
	const ROOT = './';
	const SITE_ROOT = '../';
	const FORM = './res/custom/form/';
}
?>