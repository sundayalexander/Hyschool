<?php
namespace App\Util;

use App\Util\Properties;


/**
*This is a property Iterator class. This class
*based on the java.util.Iterator class, this
*class is used to iterate through the properties.
*@author Amowe Sunday Alexander
*@version 1.0
*/



final class Iterator {
	//This holds the properties object
	private $prop, $size, $keys, $values;
	private static $key = 0, $value = 0;
	
	
	//Constructor method
	public function __construct(Properties $prop){
		$this->prop = $prop;
		$this->size = $this->prop->size();
		$this->keys = $this->prop->getKeys();
		$this->values = $this->prop->getValues();
		Iterator::$key = 0;
		Iterator::$value = 0;
	}
	
	/**
	*This method checks if there is still 
	*element to loop through.
	*@return bool: This is set to true if
	*there is still element to loop through
	*otherwise false.
	*/
	public function hasNext(){
		if(Iterator::$key < $this->size || Iterator::$value < $this->size){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	*This method returns the key of 
	*the next element in properties object.
	*@return string: This is set to the key
	*the next element.
	*/
	public function nextKey(){
		if(Iterator::$key < $this->size){
			$key = $this->keys[Iterator::$key];
			Iterator::$key++;
			return $key;
		}
	}
	
	/**
	*This method returns the value of 
	*the next element in properties object.
	*@return string: This is set to the value
	*the next element.
	*/
	public function nextValue(){
		if(Iterator::$value < $this->size){
			$value = $this->values[Iterator::$value];
			Iterator::$value++;
			return $value;
		}
	}
	
}

?>