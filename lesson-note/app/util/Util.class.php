<?php 
namespace App\Util;


/*
this util class 
contains a collection
of functions that are
use for various purposes
*/

//use the required namespace here
use Database\DB;
use App\Util\Properties;

//Include the required files here


class Util{
	//Properties
	private $db;
	
	//Constructor
	function __construct(){
			$this->db = new DB();
	}

    /**
     * @param array $file this is the file to be uploaded.
     * @param $des This is the destination where
     *the uploaded file will be saved
     * @return bool this is set to true if the file was
     * successfully uploaded otherwise false.
     */
    public static function fileUpload($file, $des){
        if(move_uploaded_file($file["tmp_name"], $des."/".$file["name"])){
            return true;

        }else{
            return false;
        }
    }
	
	 
	 public function sub_class_desig($string){  // formerly used to output subject-class designation, but dormant now 
		 $str = explode(",",$string);
		 foreach ($str as $string_array){
		 $str_expld = explode("&",$string_array);
		 $str1 = $str_expld[0];
		 $str2 = $str_expld[1];
		 $sub = mysql_fetch_assoc(mysql_query("select * from tbl_add_subject where sb_id = $str1"));
		 $class = mysql_fetch_assoc(mysql_query("select * from tbl_add_class where c_id = $str2"));
   		 $designation = 'Taking '.$sub['sb_name'].' for class '.$class['c_name'].'<br>';
		 echo $designation;		 
			 } //ends foreach
		 } //ends function
		 
		 


	 public function class_desig($string){	// formally used to output teacher class designation, but dormant now		 
		 $str = explode(",",$string);
		 foreach($str as $cls){		
   		 $class = mysql_fetch_assoc(mysql_query("select * from tbl_add_class where c_id = $cls"));
		 $designation = 'Class Teacher  '. 'for class '.$class['c_name'].'<br>';
		 echo $designation;		 
			 }
		 } //ends function
		 
		 
	public function class_id_to_class_name($class_id){ // takes a class id and return the class name
			 $qry = mysql_fetch_assoc(mysql_query("select * from tbl_add_class where c_id = $class_id"));
			 return $qry['c_name']; 
				 }
		 
	public function class_name_to_class_id($class_name){ // takes a class name and return the class id
			 $qry = mysql_fetch_assoc(mysql_query("select * from tbl_add_class where c_name = '$class_name'"));
			 return $qry['c_id']; 
     			 }
		 
	public function teacher_id_to_name($t_id){ //takes a teacher id and return the teacher's name
			 $qry = mysql_fetch_assoc(mysql_query("select * from tbl_add_teacher where teacher_id = $t_id"));
			 return $qry['t_name']; 
				 }
			 
    public function if_exist($qry){ // check if a row exist
			$rows = mysql_num_rows(mysql_query($qry));
			return $rows;
				 }
				 
	public function field_exist($qry,$link){ // check if a field exist in table
			$rows = mysql_num_rows(mysql_query($qry));
			if($rows>0){
				return true;
				}
				else{
					return false;
					}
				 }
				 
    public function fetch($qry,$field){ //fetch a row into an associative array
			 $rst = mysql_fetch_assoc(mysql_query($qry));
			 return $rst[$field];
	}
					 
					 
					 
					 // this function takes uses a value to
					 // return a field from the row that has th value
    public function id_to_field($table,$column,$field,$rtnvalue){ 
		 $exist = $this->db->exist($table, new Properties(array($column => $field)));
		  var_dump($exist);
		 $qry = $this->db->getResult();
		
		 return $qry[$rtnvalue];
	}
				 
				 
				 // uploads an image
				 
	public function uploadImage(){
	if((!empty($_FILES["fTrImage"])) && ($_FILES['fTrImage']['error'] == 0)) {
	  $filename = basename($_FILES['fTrImage']['name']);
	  $ext = substr($filename, strrpos($filename, '.') + 1);
	  if(($ext == "jpg" && $_FILES["fTrImage"]["type"] == 'image/jpeg') || ($ext == "jpeg" && $_FILES["fTrImage"]["type"] == 'image/jpeg')||($ext == "png" && $_FILES["fTrImage"]["type"] == 'image/png') || ($ext == "gif" && $_FILES["fTrImage"]["type"] == 'image/gif')){   
	  	$temp = explode(".",$_FILES["fTrImage"]["name"]);
	  	$newfilename = rand(1,99999) . uniqid() . '.' .end($temp);
	  	move_uploaded_file($_FILES["fTrImage"]["tmp_name"], ROOT_PATH . '/img/upload/' . $newfilename);
		return $newfilename;
	  }
	  else{
	  	return '';
	  }
	}
	return '';
}
				 
				 
	public function broadcast($receiver_array,$from,$title,$message){
		foreach($receiver_array as $to){		
		mysql_query("insert into msg values('','$from','$to','$title','$message','0',NOW())");
		}
		}		 



	public function send_mail($from,$to,$title,$message){
		mysql_query("insert into msg values('','$from','$to','$title','$message','0',NOW())");
		
		}		


		public function broad(){



		}

		public function report_completed($class){
			$session = $this->id_to_field("tbl_add_school","school_id",$_SESSION['sch_id'],"session");
			 $term = $this->id_to_field("tbl_add_school","school_id",$_SESSION['sch_id'],"term");
			$sub = mysql_num_rows(mysql_query("select * from cst where sch_id = '{$_SESSION['sch_id']}' and class_id = '$class'"));

			$sub_report = mysql_num_rows(mysql_query("select distinct sub_id from terminal_report where sch_id = '{$_SESSION['sch_id']}' and class_id = '$class' and session = '$session' and term = '$term'  "));
			//echo $sub_report."--". $sub."--".$class;
			if ($sub == $sub_report) {
				return 1;
			}
			else{return 0;}

		}

		function num_row($qry){
			$q = mysql_query($qry);
			return mysql_num_rows($q);

		}



	
} // ends class
	


?>