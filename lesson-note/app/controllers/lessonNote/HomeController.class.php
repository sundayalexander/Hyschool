<?php
/**
 *This is an HomeController class.
 * This class is required by lessonNote
 * index.php page, all business logic
 * should be done here.
 *@author Amowe Sunday Alexander
 *@version 1.0
 */

namespace App\Controllers\LessonNote;


//use the required namespace
use App\Util\URL;
use \MySQLi_Sql_Exception;
use App\Controllers\ControllerAdapter;
use App\Util\Properties;
use App\Util\Util;
use Database\DB;
use Database\Schema;
use Database\Table;


//include the neccessary files here
include_once 'app/controllers/ControllerAdapter.class.php';
//include_once '../database/DB.class.php';
//include_once '../app/util/Properties.class.php';
include_once 'app/util/Util.class.php';
include_once 'database/Schema.class.php';






final class HomeController extends ControllerAdapter {
    //Properties goes here!
    private $db; //database object
	private $prop; //Properties object
	
	/**
	*This constructor initialize the dabase object
	*/
	function __construct(){
		$this->db = new DB();
	}
	
	/**
	*This method redirect the client to an
	*appropriate page.
	*@param array $session This is the
	*session received by the server
	*/
	public function redirect(array $session){
		$this->prop = new Properties($session);
		if($this->prop->contains(array('login_type')) && ($this->prop->getProperty('login_type') != 'admin' &&
                $this->prop->getProperty('login_type') != 'teacher')){
            header("Location: ../logout.php");
            die();
		}

	}
	
	/**
	*This method return the list of register subjects.
	*@return string this is set to an html option tag
	* of all register subjects.
	*/
	public function getSubject(){
		$subjects = $this->db->fetchAll("tbl_add_subject",array("sb_name"),true);
		$output = "";
		while($subjects->hasNext()){
			$subject = $subjects->next("sb_name");
			 $output .= "<option value='{$subject}'>".$subject."</option>";
		}
		return $output;
	}

    /**
     * This method save the lesson notes
     * into the database.
     * @param Properties $prop
     * @return bool|\Database\This|void
     * @throws \Database\InvalidArgumentException
     */
    public function post(Properties $prop){
		$table = new Table("tbl_lesson_note");
		$table->setInt("id",255);
		$table->setString("subject");
		$table->setString("title");
		$table->setString("note");
        $table->setString("note_url");
		$table->setInt("teacher_id",255,false);
		$table->setTimestamp("submitted_date");
		$table->setInt("status",1,false);
		$table->setString("tagNote");
		$table->primaryKey("id");
		Schema::createTable($table);
        if($prop->contains(array("title","subject","note","tagNote"))){
            $session = $prop->getProperty("session");
            $id = $session["login_type"] == "admin"?$session["objLogin"]["aid"]:
                $session["objLogin"]["teacher_id"];
            $prop->put("teacher_id",$id);
			if(Util::fileUpload($prop->getProperty("note"),URL::STORAGE."docs/lesson-note")){
				$save = new Properties();
				$note = $prop->getProperty("note");
				$save->put("title",$prop->getProperty("title"));
				$save->put("subject",$prop->getProperty("subject"));
				$save->put("tagNote",$prop->getProperty("tagNote"));
				$save->put("teacher_id",$prop->getProperty("teacher_id"));
				$save->put("note_url",URL::STORAGE."docs/lesson-note/".$note["name"]);
                $save->put("note", substr($note["name"],0,strlen($note["name"])-4));
				$save->put("status",0);
				return $this->db->save("tbl_lesson_note", $save);				
			}else{
				return false;
			}

        }
		return false;
    }

    /**
     * This method return the number
     * pending notes.
     * @param Properties $prop
     * @return mixed
     * @throws \ReflectionException
     */
    public function pendingNote(Properties $prop){
        if($prop->getProperty("login_type") == "admin"){
           if($this->db->exist("tbl_lesson_note",
                new Properties(array("status"=>0)))){
               return $this->db->size();
           }
        }else{
            $input = array("status"=>0,"teacher_id"=>$prop->getProperty("teacher_id"));
            if($this->db->exist("tbl_lesson_note",
                new Properties($input))){
                return $this->db->size();
            }
        }
    }

    /**
     * This method return the number
     * of rejected notes
     * @param Properties $prop
     * @return mixed
     * @throws \ReflectionException
     */
    public function rejectedNote(Properties $prop){
        if($prop->getProperty("login_type") == "admin"){
            if($this->db->exist("tbl_lesson_note",
                new Properties(array("status"=>2)))){
                return $this->db->size();
            }
        }else{
            $input = array("status"=>2,"teacher_id"=>$prop->getProperty("teacher_id"));
            if($this->db->exist("tbl_lesson_note",
                new Properties($input))){
                return $this->db->size();
            }
        }
    }

    /**
     * This method returns the
     * number of approved notes.
     * @param Properties $prop
     * @return mixed
     * @throws \ReflectionException
     */
    public function approvedNote(Properties $prop){
        if($prop->getProperty("login_type") == "admin"){
            if($this->db->exist("tbl_lesson_note",
                new Properties(array("status"=>1)))){
                return $this->db->size();
            }
        }else{
            $input = array("status"=>1,"teacher_id"=>$prop->getProperty("teacher_id"));
            if($this->db->exist("tbl_lesson_note",
                new Properties($input))){
                return $this->db->size();
            }
        }
    }


}