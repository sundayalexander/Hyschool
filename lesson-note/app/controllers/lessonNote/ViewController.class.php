<?php
/**
*This is a ViewController class.
* This class is required by lesson-note.php
 * page, all business logic
* should be done here.
*@author Amowe Sunday Alexander
*@version 1.0
*/

namespace App\Controllers\LessonNote;


//use the required namespace
use \MySQLi_Sql_Exception;
use App\Controllers\ControllerAdapter;
use App\Util\Properties;
use App\Util\Util;
use Database\DB;
use Database\DataIterator;


//include the neccessary files here
include_once 'app/controllers/ControllerAdapter.class.php';
//include_once '../database/DB.class.php';
//include_once '../app/util/Properties.class.php';
//include_once 'app/util/Util.class.php';


final class ViewController extends ControllerAdapter {
    //Properties goes here!
    private $db; //database object
    private static $counter; //Properties object

    /**
     *This constructor initialize the database object
     */
    function __construct(){
        $this->db = new DB();
        ViewController::$counter = 0;
    }

    /**
     * This method returns the list
     * of all notes in the db.
     * @param Properties $prop
     * @return mixed
     */
    public function all(Properties $prop){
        ViewController::$counter = 0;
        $output = "";
        if($prop->getProperty("login_type") == "admin"){
           $it = $this->db->fetchAll("tbl_lesson_note",array("*"));

            while($it->hasNext()){
                ViewController::$counter++;
                $author = $this->db->search("tbl_add_teacher",new Properties(
                    array("teacher_id"=>$it->next("teacher_id"),"field"=>"t_name")),"=");
                $output .= "<tr title='{$it->next("tagNote")}'><td>".ViewController::$counter."</td>";
                $output .= "<td>{$it->next("title")}</td>";
                $output .= "<td>{$it->next("subject")}</td>";
                $output .= "<td>{$author->next("t_name")}</td>";
                $output .= "<td>{$it->next("submitted_date")}</td>";
                $note = $it->next("note_url");
                $output .= "<td class=\"center d-lg-none\"><a class='fa fa-arrow-circle-o-down' href='{$note}'
                    target='_blank' download='{$note}'> {$it->next("note")}</a> </td>";
                $state = $it->next("status");

                if($state == 0){
                    $status = "<span class='fa fa-clock-o' style='color: orange'> Pending</span>";
                }else if($state == 1){
                    $status =  "<span class='fa fa-check-circle text-success' > Approved</span>";
                }else{
                    $status = "<span class='fa fa-warning text-danger'> Rejected</span>";
                }
                $id = $it->next("id");
                $output .= "<td class=\"center d-lg-none\">{$status}
                <span class='fa fa-check-square-o action' title='Approve Note' onclick='approveNote(this,{$id})'>
                </span>
                <span class='fa fa-warning action' title='Reject Note' onclick='rejectNote(this,{$id})'>
                </span></td></tr>";
            }
        }else{
            $input = array("teacher_id"=>$prop->getProperty("teacher_id"));
            $it = $this->db->search("tbl_lesson_note",new Properties($input),"=");
            while($it->hasNext()){
                ViewController::$counter++;
                $author = $this->db->search("tbl_add_teacher",new Properties(
                    array("teacher_id"=>$it->next("teacher_id"),"field"=>"t_name")),"=");
                $output .= "<tr title='{$it->next("tagNote")}'><td>".ViewController::$counter."</td>";
                $output .= "<td>{$it->next("title")}</td>";
                $output .= "<td>{$it->next("subject")}</td>";
                $output .= "<td>{$author->next("t_name")}</td>";
                $output .= "<td>{$it->next("submitted_date")}</td>";
                $note = $it->next("note_url");
                $output .= "<td class=\"center d-lg-none\"><a class='fa fa-arrow-circle-o-down' href='{$note}'
                    target='_blank' download='{$note}'> {$it->next("note")}</a> </td>";
                $state = $it->next("status");
                if($state == 0){
                    $status = "<span class='fa fa-clock-o' style='color: orange'> Pending</span>";
                }else if($state == 1){
                    $status =  "<span class='fa fa-check-circle text-success'> Approved</span>";
                }else{
                    $status = "<span class='fa fa-warning text-danger'> Rejected</span>";
                }
                $output .= "<td class=\"center d-lg-none\">{$status}</td></tr>";
            }
        }
        return $output;
    }

    /**
     * This method returns the list
     * of pending notes in the db.
     * @param Properties $prop
     * @return mixed
     */
    public function pending(Properties $prop){
        ViewController::$counter = 0;
        $output = "";
        if($prop->getProperty("login_type") == "admin"){
            $input = array("status"=>0);
            $it = $this->db->search("tbl_lesson_note",new Properties($input),"=");
            while($it->hasNext()){
                ViewController::$counter++;
                $author = $this->db->search("tbl_add_teacher",new Properties(
                    array("teacher_id"=>$it->next("teacher_id"),"field"=>"t_name")),"=");
                $output .= "<tr title='{$it->next("tagNote")}'><td>".ViewController::$counter."</td>";
                $output .= "<td>{$it->next("title")}</td>";
                $output .= "<td>{$it->next("subject")}</td>";
                $output .= "<td>{$author->next("t_name")}</td>";
                $output .= "<td>{$it->next("submitted_date")}</td>";
                $note = $it->next("note_url");
                $output .= "<td class=\"center d-lg-none\"><a class='fa fa-arrow-circle-o-down' href='{$note}'
                    target='_blank' download='{$note}'> {$it->next("note")}</a> </td>";
                $state = $it->next("status");
                if($state == 0){
                    $status = "<span class='fa fa-clock-o' style='color: orange'> Pending</span>";
                }else if($state == 1){
                    $status =  "<span class='fa fa-check-circle text-success'> Approved</span>";
                }else{
                    $status = "<span class='fa fa-warning text-danger'> Rejected</span>";
                }
                $id = $it->next("id");
                $output .= "<td class=\"center d-lg-none\">{$status}
                <span class='fa fa-check-square-o action' title='Approve Note' onclick='approveNote(this,{$id},\"pending\")'>
                </span>
                <span class='fa fa-warning action' title='Reject Note' onclick='rejectNote(this,{$id},\"pending\")'>
                </span></td></tr>";
            }
        }else{
            $input = array("status"=>0,"teacher_id"=>$prop->getProperty("teacher_id"));
            $it = $this->db->search("tbl_lesson_note",new Properties($input),"=");
            while($it->hasNext()){
                ViewController::$counter++;
                $author = $this->db->search("tbl_add_teacher",new Properties(
                    array("teacher_id"=>$it->next("teacher_id"),"field"=>"t_name")),"=");
                $output .= "<tr title='{$it->next("tagNote")}'><td>".ViewController::$counter."</td>";
                $output .= "<td>{$it->next("title")}</td>";
                $output .= "<td>{$it->next("subject")}</td>";
                $output .= "<td>{$author->next("t_name")}</td>";
                $output .= "<td>{$it->next("submitted_date")}</td>";
                $note = $it->next("note_url");
                $output .= "<td class=\"center d-lg-none\"><a class='fa fa-arrow-circle-o-down' href='{$note}'
                    target='_blank' download='{$note}'> {$it->next("note")}</a> </td>";
                $state = $it->next("status");
                if($state == 0){
                    $status = "<span class='fa fa-clock-o' style='color: orange'> Pending</span>";
                }else if($state == 1){
                    $status =  "<span class='fa fa-check-circle text-success'> Approved</span>";
                }else{
                    $status = "<span class='fa fa-warning text-danger'> Rejected</span>";
                }
                $output .= "<td class=\"center d-lg-none\">{$status}</td></tr>";
            }
        }
        return $output;
    }
    /**
 * This method returns the list
 * of pending notes in the db.
 * @param Properties $prop
 * @return mixed
 */
    public function approved(Properties $prop){
        ViewController::$counter = 0;
        $output = "";
        if($prop->getProperty("login_type") == "admin"){
            $input = array("status"=>1);
            $it = $this->db->search("tbl_lesson_note",new Properties($input),"=");
            while($it->hasNext()){
                ViewController::$counter++;
                $author = $this->db->search("tbl_add_teacher",new Properties(
                    array("teacher_id"=>$it->next("teacher_id"),"field"=>"t_name")),"=");
                $output .= "<tr title='{$it->next("tagNote")}'><td>".ViewController::$counter."</td>";
                $output .= "<td>{$it->next("title")}</td>";
                $output .= "<td>{$it->next("subject")}</td>";
                $output .= "<td>{$author->next("t_name")}</td>";
                $output .= "<td>{$it->next("submitted_date")}</td>";
                $note = $it->next("note_url");
                $output .= "<td class=\"center d-lg-none\"><a class='fa fa-arrow-circle-o-down' href='{$note}'
                    target='_blank' download='{$note}'> {$it->next("note")}</a> </td>";
                $state = $it->next("status");
                if($state == 0){
                    $status = "<span class='fa fa-clock-o' style='color: orange'> Pending</span>";
                }else if($state == 1){
                    $status =  "<span class='fa fa-check-circle text-success'> Approved</span>";
                }else{
                    $status = "<span class='fa fa-warning text-danger'> Rejected</span>";
                }
                $id = $it->next("id");
                $output .= "<td class=\"center d-lg-none\">{$status}
                <span class='fa fa-check-square-o action' title='Approve Note' onclick='approveNote(this,{$id},\"approved\")'>
                </span>
                <span class='fa fa-warning action' title='Reject Note' onclick='rejectNote(this,{$id},\"approved\")'>
                </span></td></tr>";
            }
        }else{
            $input = array("status"=>1,"teacher_id"=>$prop->getProperty("teacher_id"));
            $it = $this->db->search("tbl_lesson_note",new Properties($input),"=");
            while($it->hasNext()){
                ViewController::$counter++;
                $author = $this->db->search("tbl_add_teacher",new Properties(
                    array("teacher_id"=>$it->next("teacher_id"),"field"=>"t_name")),"=");
                $output .= "<tr title='{$it->next("tagNote")}'><td>".ViewController::$counter."</td>";
                $output .= "<td>{$it->next("title")}</td>";
                $output .= "<td>{$it->next("subject")}</td>";
                $output .= "<td>{$author->next("t_name")}</td>";
                $output .= "<td>{$it->next("submitted_date")}</td>";
                $note = $it->next("note_url");
                $output .= "<td class=\"center d-lg-none\"><a class='fa fa-arrow-circle-o-down' href='{$note}'
                    target='_blank' download='{$note}'> {$it->next("note")}</a> </td>";
                $state = $it->next("status");
                if($state == 0){
                    $status = "<span class='fa fa-clock-o' style='color: orange'> Pending</span>";
                }else if($state == 1){
                    $status =  "<span class='fa fa-check-circle text-success'> Approved</span>";
                }else{
                    $status = "<span class='fa fa-warning text-danger'> Rejected</span>";
                }
                $output .= "<td class=\"center d-lg-none\">{$status}</td></tr>";
            }
        }
        return $output;
    }

    /**
     * This method returns the list
     * of pending notes in the db.
     * @param Properties $prop
     * @return mixed
     */
    public function rejected(Properties $prop){
        ViewController::$counter = 0;
        $output = "";
        if($prop->getProperty("login_type") == "admin"){
            $input = array("status"=>2);
            $it = $this->db->search("tbl_lesson_note",new Properties($input),"=");
            while($it->hasNext()){
                ViewController::$counter++;
                $author = $this->db->search("tbl_add_teacher",new Properties(
                    array("teacher_id"=>$it->next("teacher_id"),"field"=>"t_name")),"=");
                $output .= "<tr title='{$it->next("tagNote")}'><td>".ViewController::$counter."</td>";
                $output .= "<td>{$it->next("title")}</td>";
                $output .= "<td>{$it->next("subject")}</td>";
                $output .= "<td>{$author->next("t_name")}</td>";
                $output .= "<td>{$it->next("submitted_date")}</td>";
                $note = $it->next("note_url");
                $output .= "<td class=\"center d-lg-none\"><a class='fa fa-arrow-circle-o-down' href='{$note}'
                    target='_blank' download='{$note}'> {$it->next("note")}</a> </td>";
                $state = $it->next("status");
                if($state == 0){
                    $status = "<span class='fa fa-clock-o' style='color: orange'> Pending</span>";
                }else if($state == 1){
                    $status =  "<span class='fa fa-check-circle text-success'> Approved</span>";
                }else{
                    $status = "<span class='fa fa-warning text-danger'> Rejected</span>";
                }
                $id = $it->next("id");
                $output .= "<td class=\"center d-lg-none\">{$status}
                <span class='fa fa-check-square-o action' title='Approve Note' onclick='approveNote(this,{$id},\"rejected\")'>
                </span>
                <span class='fa fa-warning action' title='Reject Note' onclick='rejectNote(this,{$id},\"rejected\")'>
                </span></td></tr>";
            }
        }else{
            $input = array("status"=>2,"teacher_id"=>$prop->getProperty("teacher_id"));
            $it = $this->db->search("tbl_lesson_note",new Properties($input),"=");
            while($it->hasNext()){
                ViewController::$counter++;
                $author = $this->db->search("tbl_add_teacher",new Properties(
                    array("teacher_id"=>$it->next("teacher_id"),"field"=>"t_name")),"=");
                $output .= "<tr title='{$it->next("tagNote")}'><td>".ViewController::$counter."</td>";
                $output .= "<td>{$it->next("title")}</td>";
                $output .= "<td>{$it->next("subject")}</td>";
                $output .= "<td>{$author->next("t_name")}</td>";
                $output .= "<td>{$it->next("submitted_date")}</td>";
                $note = $it->next("note_url");
                $output .= "<td class=\"center d-lg-none\"><a class='fa fa-arrow-circle-o-down' href='{$note}'
                    target='_blank' download='{$note}'> {$it->next("note")}</a> </td>";
                $state = $it->next("status");
                if($state == 0){
                    $status = "<span class='fa fa-clock-o' style='color: orange'> Pending</span>";
                }else if($state == 1){
                    $status =  "<span class='fa fa-check-circle text-success'> Approved</span>";
                }else{
                    $status = "<span class='fa fa-warning text-danger'> Rejected</span>";
                }
                $output .= "<td class=\"center d-lg-none\">{$status}</td></tr>";
            }
        }
        return $output;
    }

    /**
     * This method approve the lesson note
     * which id is sent by the ajax request.
     * @param $id this is the id of the lesson
     * note to approve
     * @return bool This is set to true if
     * the lesson note approved, otherwise
     * false.
     */
    public function ajaxApprove($id){
        return $this->db->update("tbl_lesson_note",new Properties(array("status" => 1)),
            new Properties(array("id"=>$id)));
    }

    /**
     * This method reject the lesson note
     * which id is sent by the ajax request.
     * @param $id this is the id of the lesson
     * note to approve
     * @return bool This is set to true if
     * the lesson note approved, otherwise
     * false.
     */
    public function ajaxReject($id){
        return $this->db->update("tbl_lesson_note",new Properties(array("status" => 2)),
            new Properties(array("id"=>$id)));
    }





}