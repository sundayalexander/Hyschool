<?php
namespace App\Controllers;
/**
*This is an abstract LoginController class. 
*@author Amowe Sunday Alexander
*@version 1.0
*/

//use the required namespace
use \MySQLi_Sql_Exception;
use App\Controllers\ControllerAdapter;
use App\Util\Properties;
use Database\DB;
use App\Util\Util;

//include the neccessary files here
include_once 'ControllerAdapter.class.php';
include_once 'database/DB.class.php';
include_once 'app/util/Properties.class.php';
include_once 'app/util/Util.class.php';


final class LoginController extends  ControllerAdapter{
	//Properties or fields goes here
	private $db; //Database connection
	private $util; //Utility Object
	
	/**
	*This constructor initialize the dabase object
	*/
	function __construct(){
		try{
			$this->db = new DB(); 
			$this->util = new Util();
		}catch(MySQLi_Sql_Exception $e){
			die("unable to connect to the server at the moment");
		}
	}
	
	/**
	*This method redirect the client to an
	*appropriate page.
	*@param Properties $session This is the
	*session received by the server
	*/
	public function redirect(Properties $session){
		
		if (!$session->isEmpty() && $session->contains(array('objLogin','login_type'))) {
			if($session->getProperty('login_type') == 'admin'){
					header("Location: lesson-note/index.php");
					die();
				}
				else if($session->getProperty('login_type') == 'teacher' ){
					$objLogin = $session->getProperty('objLogin');
					if ($objLogin['sch_id'] == 0) {
					header("Location: lesson-note/index.php");
					die();
					}else{
					header("Location: lesson-note/index.php");
					die();
				}
				}
				else if($session->getProperty('login_type') == 'student'){
					header("Location: s_dashboard.php");
					die();
				}
				else if($session->getProperty('login_type') == 'parents'){
					header("Location: p_dashboard.php");
					die();
				}
				else if($session->getProperty('login_type') == 'accountant'){
					header("Location: a_dashboard.php");
					die();
				}
				else if($session->getProperty('login_type') == 'librarian'){
					header("Location: l_dashboard.php");
					die();
				}
		}
	}

    /**
     *This method performs login operation and
     *set the necessary sessions.
     * @param Properties $param This is the
     *POST request parameter sent by the client.
     * @return string
     * @throws \ReflectionException
     */
	
	public function login(Properties $param){
		$msg = 'none';
		if(!$param->isEmpty() && $param->contains(array('username','password')) && 
		$param->getProperty('username') != ''  && $param->getProperty('password') != ''){
			
			$login = false;
			switch($param->getProperty('ddlLoginType')){
				//Admin login
				case 'admin':
					$input = array("a_email" => $param->getProperty('username'), "password" => $param->getProperty('password'));
					$login = $this->db->exist("tbl_admin_login",new Properties($input));
				break;
				
				//Teacher login
				case 'teacher':
					 if(is_int($param->getProperty('username'))) {
						 $input = array("t_phone" => $param->getProperty('username'), "t_password" => $param->getProperty('password'));
						 $login = $this->db->exist("tbl_add_teacher",new Properties($input));
					 }else{
						 $input = array("t_email" => $param->getProperty('username'), "t_password" => $param->getProperty('password'));
						 $login = $this->db->exist("tbl_add_teacher",new Properties($input)); 
					 }
					 
				break;
				
				//Student login
				case 'student':
					 if(is_numeric($_POST['username'])) {
						 $input = array("s_contact" => $param->getProperty('username'), "s_password" => $param->getProperty('password'));
						 $login = $this->db->exist("tbl_add_student",new Properties($input));
					 }else{
						 $input = array("s_email" => $param->getProperty('username'), "s_password" => $param->getProperty('password'));
						 $login = $this->db->exist("tbl_add_student",new Properties($input)); 
					 }
					 
				break;
				
				//Parent login
				case 'parents':
					 if(is_numeric($_POST['username'])) {
						 $input = array("p_contact" => $param->getProperty('username'), "p_password" => $param->getProperty('password'));
						 $login = $this->db->exist("tbl_add_parent",new Properties($input));
					 }else{
						 $input = array("p_email" => $param->getProperty('username'), "p_password" => $param->getProperty('password'));
						 $login = $this->db->exist("tbl_add_parent",new Properties($input)); 
					 }
					 
				break;
				
				//Employee login
				case 'employee':
					 $input = array("u_email" => $param->getProperty('username'), "u_password" => $param->getProperty('password'));
					 $login = $this->db->exist("tbl_add_user",new Properties($input)); 
				break;
				
				//Librarian login
				case 'librarian':
					 $input = array("u_email" => $param->getProperty('username'), "u_password" => $param->getProperty('password'));
					 $login = $this->db->exist("tbl_add_user",new Properties($input)); 
				break;
				
				//Librarian login
				case 'accountant':
					 $input = array("u_email" => $param->getProperty('username'), "u_password" => $param->getProperty('password'));
					 $login = $this->db->exist("tbl_add_user",new Properties($input)); 
				break;
			}
			
			
			if($login){
				//here success
				$_SESSION['objLogin'] = $this->db->getResult();
				$_SESSION['login_type'] = $param->getProperty('ddlLoginType');
				$_SESSION['sch_id'] = $_SESSION['objLogin']['sch_id'];
				if($param->getProperty('ddlLoginType') == 'admin'){
					$_SESSION['active'] =$this->util->id_to_field("tbl_add_school","school_id",$_SESSION['sch_id'],"Active");
					
					$input = array("school_id" => $_SESSION['sch_id']);
					$this->db->exist("tbl_add_school",new Properties($input));
					
					$mdl = $this->db->getResult();
					$_SESSION['module'] = $mdl['assessment_module'];
					
					header("Location: lesson-note/index.php");
					die();
				}
				else if($param->getProperty('ddlLoginType') == 'teacher'){
					if ($_SESSION['objLogin']['sch_id'] == 0) {
					header("Location: lesson-note/index.php");
					die();
					}else{
						$mdl = $this->db->getResult();
					$_SESSION['module'] = $mdl['assessment_module'];
					header("Location: lesson-note/index.php");
					die();
				}
				}
				else if($param->getProperty('ddlLoginType') == 'student'){
					header("Location: test/s_dashboard.php");
					die();
				}
				else if($param->getProperty('ddlLoginType') == 'parents'){
					header("Location: test/p_dashboard.php");
					die();
				}
				else if($param->getProperty('ddlLoginType') == 'accountant'){
					header("Location: test/a_dashboard.php");
					die();
				}
				else if($param->getProperty('ddlLoginType') == 'librarian'){
					header("Location: test/l_dashboard.php");
					die();
				}
			}
			else{
				$msg = 'block';
			}
		}
		return $msg;

	}
		
}

