<?php 
//use required namespace
use App\Controllers\LessonNote\HomeController;
use App\Controllers\LessonNote\ViewController;
use App\Util\Properties;
use App\Util\URL;

//file inclusion goes here!
require_once('res/layout/header.php');
require_once('app/controllers/lessonNote/HomeController.class.php');
require_once('app/controllers/lessonNote/ViewController.class.php');
$date = date("d/m/Y");
$notify = new HomeController();
$notify->redirect($_SESSION);

$prop = new Properties();
$prop->put("login_type",$_SESSION["login_type"]);
//for teachers
if($prop->getProperty("login_type") == "teacher"){
    $prop->put("teacher_id",$_SESSION["objLogin"]["teacher_id"]);
}
$pending = $notify->pendingNote($prop);
$approved = $notify->approvedNote($prop);
$rejected = $notify->rejectedNote($prop);

//display the notes
$view = new ViewController();
if(isset($_GET["v"]) && str_replace("'","",urldecode($_GET["v"])) == "all"){
    $datas = $view->all($prop);
}else if(isset($_GET["v"]) && str_replace("'","",urldecode($_GET["v"])) == "approved"){
    $datas = $view->approved($prop);
}else if(isset($_GET["v"]) && str_replace("'","",urldecode($_GET["v"])) == "rejected"){
    $datas = $view->rejected($prop);
}else if(isset($_GET["v"]) && str_replace("'","",urldecode($_GET["v"])) == "pending"){
    $datas = $view->pending($prop);
}else{
   $datas = $view->all($prop);
}

?>


<!-- Admin Extension Skin CSS -->
<link rel="stylesheet" href="../res/assets/css/skins/extension.css">
<link rel="stylesheet" href="../res/assets/css/skins/default.css">

<!-- Admin Extension Specific Page Vendor CSS -->
<link rel="stylesheet" href="<?=URL::ASSETS?>vendor/select2/css/select2.css" />
<link rel="stylesheet" href="<?=URL::ASSETS?>vendor/select2-bootstrap-theme/select2-bootstrap.css" />
<link rel="stylesheet" href="<?=URL::ASSETS?>vendor/jquery-datatables/media/css/dataTables.bootstrap4.css" />




<style type="text/css">
a{

  color:#555;
  font-weight: bolder;
}
.menu-items{
  margin:20px 10px;
  float: left;
   width: 200px;
   background-color: #31516A;
   color: #fc3;
   padding: 10px 0px;
   text-align: center;
   font-size: 14px;

}

 .summary a{
   display:block;
   width: 200px;
   background-color: #31516A;
   color: #fff;
   font-size: 14px;
   position: relative;
   padding: 30px 0px;
   text-align: center;
   float: left;
   margin: 20px 20px 0px 0px;
   border-radius: 4%;

}
.summary:hover {
	cursor: pointer;	
}
.summary:hover em {
	text-decoration: underline;
}
 
.material-icons {
  font-size: 14px;
  font-weight: bolder;
  bottom: -20px;
  margin: auto;
}
table tbody tr td .action{
    visibility: hidden;
    padding: 4px 10px;
}
table tbody tr:hover td .action {
    float: right;
    visibility: visible;
}
table tbody tr td .action:hover{
    color: cadetblue;
}
</style>

<div class="content" style="line-height: 1.42857143; padding-left: 1.5em;">

  <!-- <div style="position:absolute;left:0px;margin-left:10px;margin-top:5px;" id="timediv">
  </div> -->
 

    <div class="row" style="margin-left:auto;margin-right:auto;width: 100%;">

       <div class="col-md-12" style="padding-right: 54px; padding-left: 10px; margin-top: 1em;  margin-bottom: 3em;">
           <div class="row panel panel-default" style="border:1px solid #ebecc1; padding: 1em;">
           <div class="panel-heading"><strong>Lesson Note Summary</strong></div>
            <div class="summary">
                <a href="lesson-note.php?v='approved'"><em>Approved Notes</em>
                    <span class="badge badge-primary" style="background-color: #fc3;"><?=$approved?></span>
                </a>
            </div>

            <div class="summary">
            	<a href="lesson-note.php?v='rejected'"><em>Rejected Notes</em>
                    <span class="badge badge-primary" style="background-color: #fc3;"><?=$rejected?></span>
                </a>
            </div>

            <div class="summary">
            	<a href="lesson-note.php?v='pending'"><em>Pending Notes</em>
                    <span class="badge badge-primary" style="background-color: #fc3;"><?=$pending?></span>
                </a>
            </div>

          <div class="summary" style="margin-right: 0px;">
          	<a href="lesson-note.php?v='all'"><em>View All Notes</em></a>
          </div><br /><br /><br /><br /><br />
      </div>

    <br />
           <div class="row">
               <div class="col">
                   <section class="card card-admin">
                       <header class="card-header">
                           <div class="card-actions">
                               <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                               <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                           </div>

                           <h4 class="card-title"><?=ucwords(str_replace("'","",$_GET["v"]))?> Lesson Note(s)</h4>
                           <hr />
                       </header>
                       <div class="card-body">
                           <table class="table table-bordered table-striped mb-0" id="datatable-tabletools">
                               <thead>
                               <tr>
                                   <th>#</th>
                                   <th>Title</th>
                                   <th>Subject</th>
                                   <th>Author</th>
                                   <th>Date</th>
                                   <th class="d-lg-none"> Note</th>
                                   <th class="d-lg-none">Status</th>
                               </tr>
                               </thead>
                               <tbody>
                               <?=$datas?>

                               </tbody>
                           </table>
                       </div>
                       <style type="text/css">
                           table  tbody tr:hover {
                               cursor: pointer;
                               background-color: #d3d3d3;
                           }

                       </style>
                   </section>
               </div>
           </div>

       </div>
    </div>
</div>
    <!-- Admin Extension Specific Page Vendor -->
    <script src="<?=URL::ASSETS?>vendor/select2/js/select2.js"></script>
    <script src="<?=URL::ASSETS?>vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
    <script src="<?=URL::ASSETS?>vendor/jquery-datatables/media/js/dataTables.bootstrap4.js"></script>
    <script src="<?=URL::ASSETS?>vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
    <script src="<?=URL::ASSETS?>vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
    <script src="<?=URL::ASSETS?>vendor/jquery-datatables/media/js/dataTables.bootstrap4.js"></script>
    <script src="<?=URL::CUSTOM?>js/lesson-note/ajax.js"></script>

    <!-- Admin Extension -->
    <script src="<?=URL::ASSETS?>js/theme.admin.extension.js"></script>

    <!-- Admin Extension Examples -->
    <script src="<?=URL::ASSETS?>js/examples/examples.datatables.default.js"></script>
    <script src="<?=URL::ASSETS?>js/examples/examples.datatables.row.with.details.js"></script>
    <script src="<?=URL::ASSETS?>js/examples/examples.datatables.tabletools.js"></script>
		
		<!-- Theme Custom -->
		<script src="../res/assets/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="../res/assets/js/theme.init.js"></script>
<?php include(URL::LAYOUT.'footer.php');?>


