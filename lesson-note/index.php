<?php 

//use required namespace
use App\Controllers\LessonNote\HomeController;
use App\Util\Properties;
use App\Util\URL;

//file inclusion goes here!
require_once('res/layout/header.php');
require_once('app/controllers/lessonNote/HomeController.class.php');


if(isset($_SESSION['login_type']) && $_SESSION['login_type'] == 'admin'){
	header("Location: lesson-note.php?v=all");
	die();
}
$date = date("d/m/Y");
$controller = new HomeController();
//$controller->redirect($_SESSION);
$subject = $controller->getSubject();

//Save the lesson note
$status = "";
if(isset($_POST["submitNote"])){
	$prop = new Properties($_POST);
	$prop->put("note",$_FILES["note"]);
	$prop->put("session",$_SESSION);
	$prop->remove("submitNote");
	$status = $controller->post($prop)?'<div class="alert alert-success">
	<strong>Congratulations! your lesson note has been successfully submitted.</strong> 
	</div>':'<div class="alert alert-danger"><strong>Oops! there is an error in your form.
	<br /> Please check your form and resubmit</strong></div>';  
}
$prop = new Properties();
$prop->put("login_type",$_SESSION["login_type"]);
//for teachers
if($prop->getProperty("login_type") == "teacher"){
    $prop->put("teacher_id",$_SESSION["objLogin"]["teacher_id"]);
}
$pending = $controller->pendingNote($prop);
$approved = $controller->approvedNote($prop);
$rejected = $controller->rejectedNote($prop);
?>
<!-- Admin Extension Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?=URL::RES;?>assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css" />

		

		<!-- Admin Extension Skin CSS -->
		<link rel="stylesheet" href="<?=URL::RES;?>assets/css/skins/extension.css">
        <link rel="stylesheet" href="<?=URL::RES;?>assets/css/skins/default.css">
        <link rel="stylesheet" href="<?=URL::RES;?>custom/css/lesson-note/style.css">		 
        
        
<style type="text/css">
a{

  color:#555;
  font-weight: bolder;
}
.menu-items{
  margin:20px 10px;
  float: left;
   width: 200px;
   background-color: #31516A;
   color: #fc3;
   padding: 10px 0px;
   text-align: center;
   font-size: 14px;

}

 .summary a{
   display:block;
   width: 200px;
   background-color: #31516A;
   color: #fff;
   font-size: 14px;
   position: relative;
   padding: 30px 0px;
   text-align: center;
   float: left;
   margin: 20px 20px 0px 0px;
   border-radius: 4%;

}
.summary:hover {
	cursor: pointer;	
}
.summary:hover em {
	text-decoration: underline;
}
 
.material-icons {
  font-size: 14px;
  font-weight: bolder;
  bottom: -20px;
  margin: auto;
}
</style>

<div class="content" style="line-height: 1.42857143; padding-left: 1.5em;">

  <!-- <div style="position:absolute;left:0px;margin-left:10px;margin-top:5px;" id="timediv">
  </div> -->
 

    <div class="row" style="margin-left:auto;margin-right:auto;width: 100%;">

       <div class="col-md-12" style="padding-right: 54px; padding-left: 10px; margin-top: 1em;  margin-bottom: 3em;">
           <div class="row panel panel-default" style="border:1px solid #ebecc1; padding: 1em;">
           <div class="panel-heading"><strong>Lesson Note Summary</strong></div>
            <div class="summary">
                <a href="lesson-note.php?v=approved"><em>Approved Notes</em>
                    <span class="badge badge-primary" style="background-color: #fc3;" id="approved"><?=$approved?></span>
                </a>
            </div>
  
            <div class="summary">
            	<a href="lesson-note.php?v=rejected"><em>Rejected Notes</em>
                    <span class="badge badge-primary" style="background-color: #fc3;" id="rejected"><?=$rejected?></span>
                </a>
            </div>
            
            <div class="summary">
            	<a href="lesson-note.php?v=pending"><em>Pending Notes</em>
                    <span class="badge badge-primary" style="background-color: #fc3;" id="pending"><?=$pending?></span>
                </a>
            </div>
  
          <div class="summary" style="margin-right: 0px;">
          	<a href="lesson-note.php?v=all"><em>View All Notes</em></a>
          </div><br /><br /><br /><br /><br />
      </div>

    <br />
    <div class="row">
        <div class="col">
            <section class="card card-admin">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                    </div>

                    
                </header>
                <div class="card-body">
                	<?=$status?>
                	<h4 class="card-title" style="display:inline-block;">Submit Lesson Note</h4>
                    <hr />
                    <form class="form-horizontal form-bordered" method="post" enctype="multipart/form-data">
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-right pt-2" for="inputDefault">Title</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" id="inputDefault" name="title" required="required" />
                            </div>
                        </div>
                        
                         <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-right pt-2">Subject </label>
                            <div class="col-lg-6">
                                <select data-plugin-selecttwo="" class="form-control populate placeholder" 
                                data-plugin-options="{ &quot;placeholder&quot;: &quot;Select a State&quot;, 
                                &quot;allowClear&quot;: true }" name="subject">
                                    <?=$subject?>   
                                </select>
                            </div>
                        </div> 
                        
                         <!-- file uploads -->
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-right pt-2">Attach Lesson Note</label>
                            <div class="col-lg-6">
                                <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden" >
                                    <div class="input-append">
                                        <div class="uneditable-input">
                                            <i class="fa fa-file fileupload-exists"></i>
                                            <span class="fileupload-preview"
                                                  style="float: right; width: 100%; padding-left: 1.2em;margin-top: -1.5em;"></span>
                                        </div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-exists fileupload-controls">Change Note</span>
                                            <span class="fileupload-new">Attach note</span>
                                            <input type="file" accept="" required="required" name="note">
                                        </span>
                                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                       <!--end of file uploads -->
                    
                    
                     <div class="form-group row">
                        <label class="col-lg-3 control-label text-lg-right pt-2" for="inputNote">Tag Note</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" id="inputNote" name="tagNote" />
                        </div>
                    </div>
                    <div class="form-group row pull-right" style="width:42%;">
                        <input type="submit" class="btn btn-lg btn-big btn-primary form" value="Submit Note" name="submitNote" />
                   	</div>
    
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>
    </div>
<!-- Theme Base, Components and Settings -->
		<script src="js/theme.js"></script>

		<!-- Admin Extension Specific Page Vendor -->
		<script src="<?=URL::RES;?>assets/vendor/autosize/autosize.js"></script>
		<script src="<?=URL::RES;?>assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>

		<!-- Admin Extension -->
		<script src="<?=URL::RES;?>assets/js/theme.admin.extension.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?=URL::RES;?>assets/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?=URL::RES;?>assets/js/theme.init.js"></script>
<?php include(URL::LAYOUT.'footer.php');?>


