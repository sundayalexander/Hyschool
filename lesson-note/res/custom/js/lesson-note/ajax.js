/** All leson note ajax function goes here */
/**
 * This method send an ajax request to the server
 * to approve the specified lesson note.
 * @param element This is the summary to
 * update.
 * @param id this is the lesson note id
 * to which the server is to approve.
 */
function approveNote(element, id) {
    //var pane = document.getElementById(element);
    $.post("ajax.php",
        {
            id: id,
            state: "approve"
        },
        function(data,status){
            var response = $.parseJSON(data);
            if(response.message == "true"){
                element.parentNode.innerHTML = "<span class='fa fa-check-circle text-success' > Approved</span>" +
                    "<span class='fa fa-check-square-o action' title='Approve Note' onclick='approveNote(this,"+response.id+")'>" +
                    "</span><span class='fa fa-warning action' title='Reject Note' onclick='rejectNote(this,"+response.id+")'>" +
                    "</span>";
            }
        });
}

/**
 * This method send and ajax request to server
 * for a particular note to be rejected.
 * @param element
 * @param id This is the id of the lesson
 * note to be rejected.
 */
function rejectNote(element,id) {
    //alert(id);
    $.post("ajax.php",
        {
            id: id,
            state: "reject"
        },
        function (data, status) {
            var response = $.parseJSON(data);
            if(response.message == "true"){
                element.parentNode.innerHTML = "<span class='fa fa-warning text-danger'> Rejected</span>" +
                    "<span class='fa fa-check-square-o action' title='Approve Note' onclick='approveNote(this,"+response.id+")'>" +
                    "</span><span class='fa fa-warning action' title='Reject Note' onclick='rejectNote(this,"+response.id+")'>" +
                    "</span>";
            }
        }
        );
}