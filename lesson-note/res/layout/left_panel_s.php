<?php
use App\Util\URL;
?>
<style>
 @media (min-width: 765px) {

    nav.sidebar.navbar.sidebar>.container .navbar-brand, .navbar>.container-fluid .navbar-brand {
      margin-left: 0px;
    }

    nav.sidebar .navbar-brand, nav.sidebar .navbar-header{
      text-align: center;
      width: 100%;
      margin-left: 0px;
    }
    
    nav.sidebar a{
      padding-right: 23px;
      font-weight: bolder;
    }

    nav.sidebar .navbar-nav > li:first-child{
/*      border-top: 1px #ebeff1 solid;
*/    }

    nav.sidebar .navbar-nav > li{
      border-bottom: 1px #ebeff1 solid;
    }

    nav.sidebar .navbar-nav .open .dropdown-menu {
      /*position: relative;*/
      float: none;
      width: auto;
      margin-top: 0;
      background-color: transparent;
      border: 0;
      -webkit-box-shadow: none;
      box-shadow: none;
    }

    nav.sidebar .navbar-collapse, nav.sidebar .container-fluid{
      padding: 0 0px 0 0px;
    }

    .navbar-inverse .navbar-nav .open .dropdown-menu>li>a {
      color: #777;
    }

    nav.sidebar{
      width: 200px;
      height: 100%;
      float: left;
      margin-bottom: 0px;
    }

    nav.sidebar li {
      width: 100%;
    }

    nav.sidebar:hover{
      margin-left: 0px;
    }

    .forAnimate{
      opacity: 0;
    }
  }
   
  @media (min-width: 1330px) {

    nav.sidebar{
      margin-left: 0px;
      float: left;
      height: 100%;
     
    }

    nav.sidebar .forAnimate{
      opacity: 1;
    }
  }

  nav.sidebar .navbar-nav .open .dropdown-menu>li>a:hover, nav.sidebar .navbar-nav .open .dropdown-menu>li>a:focus {
    color: #CCC;
    background-color: transparent;
  }

  nav:hover .forAnimate{
    opacity: 1;
  }
  .navbar{
    border-color: #ebeff1;
  }
  

</style>






<nav id="navi" class="navbar navbar-default sidebar" role="navigation" style="background-color: #ebeff1; min-height: 100%; height:100%;bottom: 0; position: fixed; border-radius: 0px;top: 60px;">
    <div class="container-fluid" >
    <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="<?=URL::SITE_ROOT;?>"><span style="font-size:16px;padding-right: 35px" class=" showopacity glyphicon glyphicon-home"></span>Home</a></li>
               
        <li><a href="<?=URL::SITE_ROOT;?>mark/student_mark_details.php"><span style="font-size:16px;padding-right: 35px" class="showopacity glyphicon glyphicon-list"></span>Assessment</a></li>

        <li><a href="<?=URL::SITE_ROOT;?>attendance/student_attendance_details.php"><span style="font-size:16px;padding-right: 35px" class="showopacity glyphicon glyphicon-list"></span>Attendance</a></li>

       

        <li><a href="<?=URL::SITE_ROOT;?>noti"><span style="font-size:16px;padding-right: 35px" class="showopacity glyphicon glyphicon-envelope"></span>Mail</a></li>

        <!-- <li><a href="<?=URL::SITE_ROOT;?>mark/mark.php"><span style="font-size:16px;padding-right: 35px" class="showopacity glyphicon glyphicon-list"></span>Assessment</a></li>

        <li><a href="<?=URL::SITE_ROOT;?>attendance/addattendance.php"><span style="font-size:16px;padding-right: 35px" class="showopacity glyphicon glyphicon-list"></span>Attendance</a></li>  -->

      </ul>
    </div>
  </div>
</nav>