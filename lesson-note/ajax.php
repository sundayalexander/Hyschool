<?php
/**
 * This file contains all ajax
 * request and responses.
 */
//use required namespace
use App\Controllers\LessonNote\ViewController;



//file inclusion goes here!
include_once 'database/DB.class.php';
include_once 'app/util/Properties.class.php';
include_once 'app/controllers/lessonNote/ViewController.class.php';
$controller = new ViewController();
if(isset($_POST["state"]) && $_POST["state"] == "approve"){
    settype($_POST["id"],"int");
    $response = array("id"=>$_POST["id"]);
    if($controller->ajaxApprove($_POST["id"])){
        $response["message"] = "true";
    }else{
        $response["message"] = "false";
    }
    echo json_encode($response);

}else if(isset($_POST["state"]) && $_POST["state"] == "reject"){
    settype($_POST["id"],"int");
    $response = array("id"=>$_POST["id"]);
    if($controller->ajaxReject($_POST["id"])){
        $response["message"] = "true";
    }else{
        $response["message"] = "false";
    }
    echo json_encode($response);
}
?>