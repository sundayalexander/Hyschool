<?php
namespace Database;
/**
*This is a database class. This class
*based on laravel eloquent class, this
*class hides the technicality of the 
*SQL  queries from the user by providing
*easy to use interface with various methods
*@author Amowe Sunday Alexander
*@version 1.0
*/

//use the required namespace
use App\Util\Properties;
use App\Util\Iterator;
use \MySQLi;
use \MySQLi_Sql_Exception;
use \InvalidArgumentException;
use Database\Connection;
use Database\DataIterator;

//include the neccessary files
include_once 'Connection.class.php';
include_once 'DataIterator.class.php';


final class DB extends Connection{
	/* Properties goes here */
	private $result; //MYSQLI query result.
	private static $count;
	private $fetchResult;
    const SQL_ASC = "ASC";
    const SQL_DESC = "DESC";
    protected $connection;
	private $size;
	const LOGIC_AND = "AND";
	const LOGIC_OR = "OR";
	
	//Constructor
	public function __construct(){
        $this->connect();
		DB::$count = 0;
		$this->db = NULL;
		$this->result = NULL;
		$this->fetchResult = NULL;



	}
	
	/**
	* This method filters the input data and the return
	*the filter result
	*@param string $input: this is the data to be filtered
	*
	*@return this is the filtered result.
	*/
	public function filter($input){
		$string = $this->con->real_escape_string($input);
		return strip_tags(trim($string));
	}

    /**
     * This method match the given data with
     * the database datas to check for existance.
     * @param string $table This is the
     * name of the table to be dropped.
     * @param Properties $input this is the input
     *data to test against the database data.
     * @return This is set to true if the data exist
     *otherwise false.
     * @throws \ReflectionException
     */
	public function exist($table, Properties $input){

        $field = $input->contains(array("field"))?$input->getProperty("field"): "";
        $input->remove("field");
		$keys = $input->getKeys();
		$where = "";
		foreach($keys as $key){
			$where .= " AND ".$key." = ?";
		}
		$query = $field != ""?"SELECT ".$field
            ." FROM ".$table." WHERE".substr($where, 4)
            :"SELECT * FROM ".$table." WHERE".substr($where, 4);
		$statement = $this->con->prepare($query);
		$values = $input->getValues();
		$type = "";
		$params = array();
		if(!$statement){
			return false;
		}
		//filter and set the data types
		for($i = 0; $i < count($values); $i++){
			if(is_int($values[$i])){
				$type .= "i";
			}else{
				$value = $values[$i];
				$values[$i] = $this->filter($value);
				$type .= "s";
			}
		}
		$params[] = &$type;
		for($i = 0; $i < count($values); $i++){
			$params[] = &$values[$i];
		}
		//bind the required parameters
		$ref    = new \ReflectionClass('mysqli_stmt'); 
		$method = $ref->getMethod("bind_param"); 
		$method->invokeArgs($statement,$params); 
		
		//execute the statement
		if($statement->execute()){
			$statement->store_result();
			$metadata = $statement->result_metadata();
			while($field = $metadata->fetch_field()){
				$param[] = &$row[$field->name];
			}
			//bind the result
			call_user_func_array(array($statement, "bind_result"),$param);
			$this->size = $statement->num_rows;
			while($statement->fetch()){
				foreach($row as $key => $val) { 
					$c[$key] = $val; 
				} 
				$this->result[] = $c; 
			}
			$statement->free_result();
			$statement->close();
			return true;
		}
		return false;
	}
	
	/**
	* This method returns the result data of the 
	* exist method if it returns true. 
	*@return This is set to the fetched data
	*otherwise NULL.
	*/
	public function getResult(){
		if(count($this->result) > 0 && DB::$count < count($this->result)){
			$result = $this->result[DB::$count];
			DB::$count++;
			return $result;
		}else{
			DB::$count = 0;
			$this->result = NULL;
			return NULL;	
		}
	}

	
	/**
	* This method select and use the specified database.
	*
	*@param $db this is the name of the database
	* to be created.
	*
	*@return this is set to true if the database
	*could be set and used otherwise false.
	*/
	public function setDatabase($db){
		if($this->con->real_query("USE ".$db)){
			$this->db = $db;
			return true;
		}else {
			return false;
		}
	}


    /**
     * This method returns a row of data fetched
     * from the database table.
     * @param string $table This is the
     * name of the table to be dropped.
     * @param array $fields this is the field name
	 * which are to be selected. .
     * @return This is set to an associative array of 
	 * fetched datas otherwise Null.
     */
    public function fetch($table, array $fields){
		$field = "";
        for($i = 0; $i < count($fields); $i++){
            $field .= ", ".$fields[$i];
        }
        $field = substr($field, 2);
		$sql = "SELECT ".$field." FROM ".$table;
        $this->fetchResult = $this->con->query($sql);
		if($this->fetchResult){
			return $this->fetchResult->fetch_assoc();
		}
		return NULL;
    }

    /**
     * This method returns a row of data fetched
     * from the database table.
     * @param string $table This is the
     * name of the table to be dropped.
     * @param Properties $fields
     * @param string $mode This the order mode of the
     * returned data.
     * @return This is set to true if the data exist
     *otherwise false.
     *eg. $fields = new Properties(array("field => "field1,field2", "orderby" => "field9,field4" "));
     *DB::fetch("user_table",$fields);
     */
    public function fetchSort( $table, Properties $fields, $mode = DB::SQL_ASC){
        $field = $fields->getProperty("field");
		$orderby = $fields->getProperty("orderby");
        for($i = 0; $i < count($fields); $i++){
            $field .= ", ".$fields[$i];
        }
        $field = substr($field, 2);
        switch($mode){
            case "ASC":
                $sql = "SELECT ".$field." FROM ".$table."ORDER BY ".$orderby.$mode;
                break;
            case "DESC":
                $sql = "SELECT ".$field." FROM ".$table."ORDER BY ".$orderby.$mode;
                break;
            default: throw new InvalidArgumentException("mode can only assume one of two state: 0-1");
        }
        $this->fetchResult = $this->con->query($sql);
		if($this->fetchResult){
			return $this->fetchResult->fetch_assoc();
		}
		return NULL;
    }

    /**
     * This method returns a row of data fetched
     * from the database table.
     * @param string $table This is the
     * name of the table to be dropped.
     * @param array $fields this is the field name
     * which are to be selected.
     * @param boolean $distinct this is use to determine
     * if to select distinct element.
     * @return \Database\DataIterator is set to an Iterator object.
     */
    public function fetchAll($table, array $fields, $distinct = false){
		$field = "";
        for($i = 0; $i < count($fields); $i++){
            $field .= ", ".$fields[$i];
        }
        $field = substr($field, 2);
		$sql = $distinct?"SELECT DISTINCT ".$field." FROM ".$table:"SELECT ".$field." FROM ".$table;
        $this->fetchResult = $this->con->query($sql);
		if($this->fetchResult){
			$resultSet = array();
			while($row = $this->fetchResult->fetch_assoc()){
				foreach($row as $key => $value){
					$resultSet[$key][] = $value; 
				}
			}
			$this->fetchResult->free();
			return new DataIterator(new Properties($resultSet));
		}
		return new DataIterator(new Properties());
    }
	
	/**
	* This method saves data into the database table.
	*@param string $table This is the 
	* name of the table to be dropped.
	*@param Properties $input this is the input
	*data to test against the database data. 
	*@return This is set to true if the data exist
	*otherwise false.
	*/
	public function save($table, Properties $input){
		$key = "";
		$value = "";
		$it = $input->iterator();
		while($it->hasNext()){
			$v = $it->nextValue();
			$key .= ", ".$it->nextKey();
			if(is_int($v)){
				$value .= ", ".$v;
			}else{
				
				$v = $this->filter($v);
				$value .= ", '{$v}'";
			}
		}
		$key = substr($key,2);
		$value = substr($value,2);
		$sql = "INSERT INTO ".$table." (".$key.") VALUES (".$value.")";
		return $this->con->real_query($sql);
	}

    /**
     * This method returns the number of selected
     * rows/records.
     * @return mixed
     */
    public function size(){
        return $this->size;
    }

    /**
     * This method search the given database
     * table for the provided needle and return
     * the specified field;
     * @param $table This is the table name.
     * @param Properties $prop This contians the
     * fields, and needles
     * @param bool $strict This is use to perform
     * strict searching using where like.
     * @param string $operator This is the maths
     * operator
     * @param string $logic This is the Logical
     * operator.
     * @return DataIterator
     */
    public function search($table, Properties $prop, $operator = "LIKE", $strict = true, $logic = "AND"){
        if($prop->contains(array("field"))){
            $field = $prop->getProperty("field");
            $sql = "SELECT ".$field." FROM ".$table;
            $prop->remove("field");
        }else{
            $sql = "SELECT * FROM ".$table;
        }
        $it = $prop->iterator();
        $key = "";
        $where = "";
        while($it->hasNext()){
            if($operator == "LIKE"){
                $where .= " ".$logic." ".$it->nextKey();
                $v = $it->nextValue();
                if(is_int($v)){
                    $where .= " LIKE ".$v;
                }else{
                    $v = $this->filter($v);
                    $where .= $strict?" LIKE '{$v}'":" LIKE '%{$v}%'";
                }
            }else{
                $where .= " ".$logic." ".$it->nextKey();
                $v = $it->nextValue();
                if(is_int($v)){
                    $where .= " ".$operator." ".$v;
                }else{
                    $v = $this->filter($v);
                    $where .= " ".$operator." '{$v}'";
                }
            }
        }
        $where = $logic == "AND"?substr($where,4):substr($where,3);
        if($operator == "LIKE"){
            $sql .= " WHERE (".$where.")";
        }else{
            $sql .= " WHERE ".$where;
        }

        //Execute the query and return the result.
        $this->fetchResult = $this->con->query($sql);
        if($this->fetchResult){
            $resultSet = array();
            while($row = $this->fetchResult->fetch_assoc()){
                foreach($row as $key => $value){
                    $resultSet[$key][] = $value;
                }
            }
            $this->fetchResult->free();
            return new DataIterator(new Properties($resultSet));
        }
        return new DataIterator(new Properties());
    }

    /**
     * This method search given database table
     * using the SQL match against function and
     * return the received data as DataIterator
     * object.
     * @param $table This is the database table.
     * @param Properties $prop
     * @return DataIterator
     */
    public function match($table, Properties $prop){
        if($prop->contains(array("field"))){
            $field = $prop->getProperty("field");
            $sql = "SELECT ".$field." FROM ".$table;
            $prop->remove("field");
        }else{
            $sql = "SELECT * FROM ".$table;
        }
        $it = $prop->iterator();
        $key = "";
        $value = "";
        while($it->hasNext()){
            $key .= ", ".$it->nextKey();
            $v = $it->nextValue();
            if(is_int($v)){
                $value .= ", ".$v;
            }else{
                $v = $this->filter($v);
                $value .= ", '{$v}'";


            }
        }
        $key = substr($key,2);
        $value = substr($value,2);
        $sql .= " WHERE MATCH (".$key.") AGAINST (".$value.")";
        //Execute the query and return the result.
        $this->fetchResult = $this->con->query($sql);
        if($this->fetchResult){
            $resultSet = array();
            while($row = $this->fetchResult->fetch_assoc()){
                foreach($row as $key => $value){
                    $resultSet[$key][] = $value;
                }
            }
            $this->fetchResult->free();
            return new DataIterator(new Properties($resultSet));
        }
        return new DataIterator(new Properties());
    }

    /**This method update the specified table
     * with the given data.
     * @param $table This is the name of the
     * table to which the data are to be
     * updated.
     * @param Properties $set This is the set
     * of columns and value to update.
     * @param Properties $where This is the set
     * of columns and value to test agianst.
     * @param string $operator This is the type
     * of operator to use.
     * @return bool
     */
    public function update($table, Properties $set, Properties $where, $operator = DB::LOGIC_AND){
        $it = $set->iterator();
        $update = "";

        while($it->hasNext()){
            $update .= ", ".$it->nextKey()." = ";
            $value = $it->nextValue();
            if(is_int($value)){
                $update .= $value;
            }else{
                $value = $this->filter($value);
                $update .= "'{$value}'";
            }
        }
        $update = substr($update,2);
        $sql = "UPDATE ".$table." SET ".$update." WHERE ";
        $update = "";
        $it = $where->iterator();
        while($it->hasNext()){
            $update .= " {$operator} ".$it->nextKey()." = ";
            $value = $it->nextValue();
            if(is_int($value)){
                $update .= $value;
            }else{
                $value = $this->filter($value);
                $update .= "'{$value}'";
            }
        }
        $update = $operator == "AND"?substr($update,4):substr($update,3);
        $sql .= $update;
        if($this->con->real_query($sql)){
            return true;
        }else{
            return false;
        }


    }





}
