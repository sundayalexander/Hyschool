<?php
$env = array(

/*
    |--------------------------------------------------------------------------
    | Database Configuration properties
    |--------------------------------------------------------------------------
    |
    | This is a database configuration file all database environmental 
	| configuration goes here. *Please do not alter this file if you do
	| not really know what is going  here. @author Amowe Sunday Alexander
    |
    | Supported: "host", "user", "password", "database", "port", "socket"
    |
    */

	"host" => "localhost",
	"user" => "root",
	"password" => "",
	"database" =>"hyschool" 

);
$GLOBALS["ENV"] = $env;

