<?php
namespace Database;
/**
 *This is a database Connection class. This class
 *based on laravel eloquent class, this
 *class hides the technicality of the
 *SQL  queries from the user by providing
 *easy to use interface with various methods
 *@author Amowe Sunday Alexander
 *@version 1.0
 */
//use the required namespace here
use App\Util\Properties;
use \MySQLi;
use \MySQLi_Sql_Exception;
use \InvalidArgumentException;

//Import the required files here
include_once 'config.ini.php';

class Connection {
    //Class properties goes here
    protected $con; //This is the MYSQLI connection object.
    protected $db; //This holds the current working database.
    protected static $conn; //This is the MYSQLI connection object.

    //Connection method
    protected function connect(){
        $prop = new Properties($GLOBALS["ENV"]);
        if($prop->contains(array("host","password","user","database","port","socket"))){
            $this->con = new MySQLi($prop->getProperty("host"),
                $prop->getProperty("user"),
                $prop->getProperty("password"),
                $prop->getProperty("database"),
                $prop->getProperty("port"),
                $prop->getProperty("socket"));
            if($this->con->connect_errno > 0){
                throw new MySQLi_Sql_Exception($this->con->connect_error);
            }
            $this->db = $prop->getProperty("database");
        }else if($prop->contains(array("host","password","user","database","port"))){
            $this->con = new MySQLi($prop->getProperty("host"),
                $prop->getProperty("user"),
                $prop->getProperty("password"),
                $prop->getProperty("database"),
                $prop->getProperty("port"));
            if($this->con->connect_errno > 0){
                throw new MySQLi_Sql_Exception($this->con->connect_error);
            }
            $this->db = $prop->getProperty("database");
        }else if($prop->contains(array("host","password","user","database"))){
            $this->con = new MySQLi($prop->getProperty("host"),
                $prop->getProperty("user"),
                $prop->getProperty("password"),
                $prop->getProperty("database"));
            if($this->con->connect_errno > 0){
                throw new MySQLi_Sql_Exception($this->con->connect_error);
            }
            $this->db = $prop->getProperty("database");
        }else if($prop->contains(array("host","password","user"))){
            $this->con = new MySQLi($prop->getProperty("host"),
                $prop->getProperty("user"),
                $prop->getProperty("password"));
            if($this->con->connect_errno > 0){
                throw new MySQLi_Sql_Exception($this->con->connect_error);
            }
        }else{
            throw new InvalidArgumentException("DB::construct requires at least host, user and password");
        }

    }


    /**
     * This is a static version of connect method
     */
    protected static function connector(){
        $prop = new Properties($GLOBALS["ENV"]);
        if($prop->contains(array("host","password","user","database","port","socket"))){
            Connection::$conn = new MySQLi($prop->getProperty("host"),
                $prop->getProperty("user"),
                $prop->getProperty("password"),
                $prop->getProperty("database"),
                $prop->getProperty("port"),
                $prop->getProperty("socket"));
            if(Connection::$conn->connect_errno > 0){
                throw new MySQLi_Sql_Exception(Connection::$conn->connect_error);
            }

        }else if($prop->contains(array("host","password","user","database","port"))){
            Connection::$conn = new MySQLi($prop->getProperty("host"),
                $prop->getProperty("user"),
                $prop->getProperty("password"),
                $prop->getProperty("database"),
                $prop->getProperty("port"));
            if(Connection::$conn->connect_errno > 0){
                throw new MySQLi_Sql_Exception(Connection::$conn->connect_error);
            }

        }else if($prop->contains(array("host","password","user","database"))){
            Connection::$conn = new MySQLi($prop->getProperty("host"),
                $prop->getProperty("user"),
                $prop->getProperty("password"),
                $prop->getProperty("database"));
            if(Connection::$conn->connect_errno > 0){
                throw new MySQLi_Sql_Exception(Connection::$conn->connect_error);
            }

        }else if($prop->contains(array("host","password","user"))){
            Connection::$conn = new MySQLi($prop->getProperty("host"),
                $prop->getProperty("user"),
                $prop->getProperty("password"));
            if(Connection::$conn->connect_errno > 0){
                throw new MySQLi_Sql_Exception(Connection::$conn->connect_error);
            }
        }else{
            throw new InvalidArgumentException("DB::construct requires at least host, user and password");
        }

    }
}