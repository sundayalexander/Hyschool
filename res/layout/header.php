<?php 
ob_start();
if(!isset($_SESSION)){
session_start();
}

//use the required namespace here
use App\Util\URL;
use Database\DB;
use App\Util\Properties;

//Include the required files here
require_once("app/util/Properties.class.php");
require_once("app/util/URL.class.php");
require_once("database/DB.class.php");

$db = new DB();
$school_image = '';
if(($row_arr = $db->fetch("tbl_add_school", array("*"))) != NULL){$school_image = '../img/upload/' . $row_arr['s_image'];}
if(!isset($_SESSION['objLogin'])){header('Location: index.php');die();}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Hyschool</title>
<link type="text/css" href="<?php echo URL::VENDOR; ?>css/style.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo URL::VENDOR; ?>js/jquery-ui.css">
<script src="<?php echo URL::VENDOR; ?>js/jquery-1.10.2.js"></script>
<script src="<?php echo URL::VENDOR; ?>js/jquery-ui.js"></script>
<script src="<?php echo URL::VENDOR; ?>js/jquery.colorbox-min.js"></script>
<script src="<?php echo URL::VENDOR; ?>js/jquery.mask.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<meta name="author" content="Hyschool.ng">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?=URL::CUSTOM?>img/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<!-- Favicon -->
        
<link rel="apple-touch-icon" sizes="57x57" href="<?=URL::CUSTOM?>img/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?=URL::CUSTOM?>img/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href=<?=URL::CUSTOM?>"img/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?=URL::CUSTOM?>img/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?=URL::CUSTOM?>img/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?=URL::CUSTOM?>img/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?=URL::CUSTOM?>img/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?=URL::CUSTOM?>img/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?=URL::CUSTOM?>img/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?=URL::CUSTOM?>img/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?=URL::CUSTOM?>img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?=URL::CUSTOM?>img/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?=URL::CUSTOM?>img/favicon/favicon-16x16.png">
<link rel="shortcut icon" href="<?=URL::CUSTOM?>img/favicon/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="<?=URL::CUSTOM?>img/favicon/apple-icon.png">
<link rel="manifest" href="<?=URL::CUSTOM?>img/favicon/manifest.json">


<link rel="stylesheet" type="text/css" href="<?php echo URL::VENDOR; ?>js/modal.css">

<script src="<?php echo URL::VENDOR; ?>js/modal.js"></script>


<link href="<?php echo URL::VENDOR; ?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo URL::VENDOR; ?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo URL::VENDOR; ?>js/common.js"></script>
<!-- Font Awesome Icons -->
<link href="<?php echo URL::VENDOR; ?>dist/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="<?php echo URL::VENDOR; ?>dist/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />

<link type="text/css" href="<?php echo URL::VENDOR; ?>css/bootstrap.css" rel="stylesheet" />
<script src="<?php echo URL::VENDOR; ?>js/common.js"></script>
<script src="<?php echo URL::VENDOR; ?>js/printThis.js"></script>
<script src="<?php echo URL::VENDOR; ?>/js/ajax.js"></script>
<script src="<?php echo URL::VENDOR; ?>/js/blockUI.js"></script>
<script src="<?php echo URL::VENDOR; ?>/js/angular.min.js"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<style>
.sidenav {
    height: 100%; /* 100% Full-height */
    width: 0; /* 0 width - change this with JavaScript */
    position: fixed; /* Stay in place */
    z-index: 1; /* Stay on top */
    top: 0;
    left: 0;
    background-color: #111; /* Black*/
    overflow-x: hidden; /* Disable horizontal scroll */
    padding-top: 60px; /* Place content 60px from the top */
    transition: 0.5s; /* 0.5 second transition effect to slide in the sidenav */
}

/* The navigation menu links */
.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
    transition: 0.3s
}

/* When you mouse over the navigation links, change their color */
.sidenav a:hover, .offcanvas a:focus{
    color: #f1f1f1;
}

/* Position and style the close button (top right corner) */
.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}

/* Style page content - use this if you want to push the page content to the right when you open the side navigation */
#main {
    transition: margin-left .5s;
    padding: 20px;
}

/* On smaller screens, where height is less than 450px, change the style of the sidenav (less padding and a smaller font size) */
@media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}
}

.user-row {
    margin-bottom: 14px;
}

.user-row:last-child {
    margin-bottom: 0;
}

.dropdown-user {
    margin: 13px 0;
    padding: 5px;
    height: 100%;
}

.dropdown-user:hover {
    cursor: pointer;
}

.table-user-information > tbody > tr {
    border-top: 1px solid rgb(221, 221, 221);
}

.table-user-information > tbody > tr:first-child {
    border-top: 0;
}


.table-user-information > tbody > tr > td {
    border-top: 0;
}
.toppad
{margin-top:20px;
}
.panel-info>.panel-heading{
	background-color:#000;
	color:#FFF;}
	
	.bio-graph-heading{
	
	
	}


    .navbar-login
{
    width: 305px;
    padding: 10px;
    padding-bottom: 0px;
}

.navbar-login-session
{
    padding: 10px;
    padding-bottom: 0px;
    padding-top: 0px;
}

.icon-size
{
    font-size: 87px;
}
	
</style>

<script type="text/javascript">

var xmlhttp = false;



if(window.ActiveXObject){
  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  else{
  xmlhttp = new XMLHttpRequest(); 
    }

function getmail(serverPage) {
var obj = document.getElementById("mail");
var page = serverPage;
xmlhttp.open("GET", serverPage);
xmlhttp.onreadystatechange = function() {
if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
obj.innerHTML = xmlhttp.responseText;
//alert(xmlhttp.responseText);
}
}
xmlhttp.send(null);


}

//updates the number of inbox messages
function inboxno() {
var obj = document.getElementById("inbox");
xmlhttp.open("GET", '../noti/count.php');
xmlhttp.onreadystatechange = function() {
if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
obj.innerHTML = xmlhttp.responseText;
check();
}
}
xmlhttp.send(null);
}

 setInterval(inboxno,1000); // Runs the function inboxno every second


function check() {
xmlhttp.open("GET", '../noti/check.php');
xmlhttp.onreadystatechange = function() {
if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {  
if(xmlhttp.responseText==1){
//alert(xmlhttp.responseText);
//$("#inbox").click();
//var audio = document.getElementById('audio');
//audio.play();
}
}
}
xmlhttp.send(null);


}

//setInterval(check,1000);


function sendmail(serverPage) {
var page = serverPage;
xmlhttp.open("GET", serverPage);
xmlhttp.onreadystatechange = function() {
if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
 alert(xmlhttp.responseText);
}
}
xmlhttp.send(null);
$("#close").click();
}

function pass(session){
return session;
}


function getcontrol(){
xmlhttp.open("GET", '../noti/db.php');
xmlhttp.onreadystatechange = function() {
if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
return xmlhttp.responseText;
}
}
xmlhttp.send(null);


}



function next(){
    alert("ssss");
var bound = $("#bound").val();
var page = $("#page").val();
var total = $("#total").val();
if(page==bound){}
else{
page = ++page;
getmail('inbox.php?page='+page);
$("#page").val(page);



var inf = page*10;
var low = inf - 10;

if (inf>total){
inf = total;
}
var info = '<b>'+low+'-'+inf+'</b>';

document.getElementById("info").innerHTML = info;
}
}



function prev(){
  var total = $("#total").val();
var page = $("#page").val();
if(page>1){
page = --page;
}
getmail('../inbox.php?page='+page);
$("#page").val(page);

var inf = page*10;
var low = inf - 10;

if (inf>total){
inf = total;
}
if(page==1){
var info = '<b>'+'1'+'-'+inf+'</b>';
}else{
var info = '<b>'+low+'-'+inf+'</b>';
}
document.getElementById("info").innerHTML = info;
//document.getElementById("page").value = page++;
}


function getcontrol(){
xmlhttp.open("GET", '../noti/db.php');
xmlhttp.onreadystatechange = function() {
if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
return xmlhttp.responseText;
}
}
xmlhttp.send(null);


}


function filter(){
  var loc = $("#loc").val();
  var sub = $("#sub").val();
   var gend = $("#gend").val();
    var exp = $("#exp").val();
var obj = document.getElementById("teacher");
var page = "ajax/filter.php";
xmlhttp.open("GET", '../ajax/filter.php'+'?subject='+sub+'&location='+loc+'&gender='+gend+'&exp='+exp);
xmlhttp.onreadystatechange = function() {
if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
obj.innerHTML = xmlhttp.responseText;
}
}
xmlhttp.send(null);
}



$("#txtParents").on("input", function(){
    alert("hi");
})

</script>



</head>
<body onLoad="">
<script type="text/javascript"><!--

refreshdiv();

// --></script>


<?php
$login_type = $_SESSION['objLogin']; 
$img_x = '';
$name_x = '';
$email_x = '';
$password_x = '';
$update_id = 0;
if($_SESSION['login_type'] == 'admin'){
	$img_x = '../img/admin.png';
	$name_x = $login_type['d_name'];
	$email_x = $login_type['a_email'];
	$password_x = $login_type['password'];
	$update_id = $login_type['aid'];
}
else if($_SESSION['login_type'] == 'teacher'){
	$img_x = '../img/upload/'.$login_type['t_photo'];
	$name_x = $login_type['t_username'];
	$email_x = $login_type['t_email'];
	$password_x = $login_type['t_password'];
	$update_id = $login_type['teacher_id'];
}
else if($_SESSION['login_type'] == 'student'){
	$img_x = '../img/upload/'.$login_type['s_image'];
	$name_x = $login_type['s_profile_name'];
	$email_x = $login_type['s_email'];
	$password_x = $login_type['s_password'];
	$update_id = $login_type['s_id'];
}
else if($_SESSION['login_type'] == 'parents'){
	$img_x = '../img/upload/'.$login_type['p_image'];
	$name_x = $login_type['p_name'];
	$email_x = $login_type['p_email'];
	$password_x = $login_type['p_password'];
	$update_id = $login_type['p_id'];
}
else if($_SESSION['login_type'] == 'accountant' || $_SESSION['login_type'] == 'librarian'){
	$img_x = '../img/upload/'.$login_type['u_image'];
	$name_x = $login_type['u_profile_name'];
	$email_x = $login_type['u_email'];
	$password_x = $login_type['u_password'];
	$update_id = $login_type['u_id'];
}

?>
<input type="hidden" id="URL::VENDOR" value="<?php echo URL::VENDOR; ?>" />

<div class="main"><div class="navbar navbar-default navbar-fixed-top" role="navigation" style="background-color: #31516A;height: 60px;padding: 0px 30px; color: #fff;position: fixed;top: 0;left: 0;
     width: 100%;">
    <div class="container" style="width: 100%"> 
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span> 
            </button>
            <a href="../index.php"  style="text-decoration: none;"> 
        <span class="glyphicon glyphicon-education" style="color: #fff; zoom:2.2;"></span><span style="color: #fff">Hyschool</span>
        </a>
        </div>
        <div class="collapse navbar-collapse">
            
            <ul class="nav navbar-nav navbar-right" style="background-color: #31516A;">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="text-decoration: none; color: #fff" >
                        <span class="glyphicon glyphicon-user"></span> 
                        <strong>Hi 
 <?php 
 if($_SESSION['login_type'] == 'admin'){
    echo $_SESSION['objLogin']['d_name'];}
    elseif ($_SESSION['login_type'] == 'teacher') {
        echo $_SESSION['objLogin']['t_username'];
    }
    elseif ($_SESSION['login_type'] == 'parents') {
        echo $_SESSION['objLogin']['p_profile_name'];
    }
    elseif ($_SESSION['login_type'] == 'student') {
        echo $_SESSION['objLogin']['s_profile_name'];
    }
 ?>
    </strong>
                       
                    </a>

                </li>
                <li>
                    <a href="../logout.php" style="text-decoration: none; color: #fc3"> logout</a>
                    </li>
                    <li>
                     <a href="../noti/index.php" style="text-decoration: none; color: #fc3" > (<div id="inbox" style="display: inline;">0</div>) <span class="fa fa-envelope"></span></a>

                </li>
                <li><a role="button" data-toggle="modal" data-target="#pwd" href="javascript:void" style="text-decoration: none;color: #fc3">Change Password</a></li>
            </ul>
        </div>
    </div>
</div>
     
</div>

<?php  
 if(isset($_SESSION['login_type']) && $_SESSION['login_type'] == 'admin'){
    include('left_panel.php');
 }
 else if(isset($_SESSION['login_type']) && $_SESSION['login_type'] == 'student'){
    include('left_panel_s.php');
}
 else if(isset($_SESSION['login_type']) && $_SESSION['login_type'] == 'teacher'){

    if ($_SESSION['objLogin']['sch_id'] == 0) {
            
        include('left_panel_f.php');
            }else{
            
            include('left_panel_t.php');
        }
    
 }
 else if(isset($_SESSION['login_type']) && $_SESSION['login_type'] == 'accountant'){
    include('left_panel_a.php');
 }
 else if(isset($_SESSION['login_type']) && $_SESSION['login_type'] == 'librarian'){
    include('left_panel_l.php');
 }
 else if(isset($_SESSION['login_type']) && $_SESSION['login_type'] == 'parents'){
    include('left_panel_p.php');
 } 
 ?>