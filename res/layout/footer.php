
</div> 
<!-- end main -->
<?php
use App\Util\URL;
 if(isset($_SESSION['login_type']) && $_SESSION['login_type'] == 'admin'){
	include('left_panel.php');
 }
 else if(isset($_SESSION['login_type']) && $_SESSION['login_type'] == 'student'){
 	include('left_panel_s.php');
}
 else if(isset($_SESSION['login_type']) && $_SESSION['login_type'] == 'teacher'){

 	if ($_SESSION['objLogin']['sch_id'] == 0) {
			
		include('left_panel_f.php');
			}else{
			
			include('left_panel_t.php');
		}
 	
 }
 else if(isset($_SESSION['login_type']) && $_SESSION['login_type'] == 'accountant'){
 	include('left_panel_a.php');
 }
 else if(isset($_SESSION['login_type']) && $_SESSION['login_type'] == 'librarian'){
	include('left_panel_l.php');
 }
 else if(isset($_SESSION['login_type']) && $_SESSION['login_type'] == 'parents'){
 	include('left_panel_p.php');
 }
?>


<!-- modal end -->
<script src="../plugins/datatable/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../plugins/datatable/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="../plugins/datatable/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="../plugins/datatable/js/dataTables.tableTools.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function () {
	$('.sakotable').dataTable({
	  "bPaginate": true,
	  "bLengthChange": true,
	  "bFilter": true,
	  "bSort": true,
	  "bInfo": true,
	  "bAutoWidth": false,
	  "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "../plugins/datatable/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
                "print",
				"csv",
				"xls",
				"pdf"
            ]
        }
	});
});

$(function () {
	$('.sakotable_with_print').dataTable({
	  "bPaginate": false,
	  "bLengthChange": true,
	  "bFilter": false,
	  "bSort": true,
	  "bInfo": false,
	  "bAutoWidth": false,
	  "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "../plugins/datatable/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
                "print",
				"csv",
				"xls",
				"pdf"
            ]
        }
	});
});

$(function () {
	$('.common_sakotable').dataTable({
	  "bPaginate": false,
	  "bLengthChange": true,
	  "bFilter": false,
	  "bSort": true,
	  "bInfo": false,
	  "bAutoWidth": false
	});
});

$(function () {
	$('.common_sakotable_routine').dataTable({
	  "bPaginate": false,
	  "bLengthChange": true,
	  "bFilter": false,
	  "bSort": false,
	  "bInfo": false,
	  "bAutoWidth": false
	});
});

$(function () {
	$('.common_sakotable_att').dataTable({
	  "bPaginate": false,
	  "bLengthChange": false,
	  "bFilter": false,
	  "bSort": false,
	  "bInfo": false,
	  "bAutoWidth": false
	});
});
</script>


<!-- BEGIN JIVOSITE CODE {literal} -->
<!-- <script type='text/javascript'>
(function(){ var widget_id = 'YnWSYWqzqp';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script> -->
<!-- {/literal} END JIVOSITE CODE -->    

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/59c245fbc28eca75e46212b5/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

</body>
</html>
 <?php include(URL::ROOT.'res/vendor/js/modaljs.php');?>

 <div class="modal fade" id="pwd">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="close">×</button>
            <h4 class="modal-title">Change Password</h4>
          </div>
          <div class="modal-body">
            <form role="form" class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-7" for="inputTo"> Password</label>
                  <div class="col-sm-10"><input type="password" class="form-control"  name="pass" id="adminpass" placeholder="New Password"></div>
                </div>
                
                
           
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal" id="close">Cancel</button> 
           
            <button type="button" class="btn btn-primary " onclick="sendmail('changepassword.php?pass='+$('#adminpass').val());" style="background-color:#ffcc33">Update Password</button>
             </form>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal compose message -->

