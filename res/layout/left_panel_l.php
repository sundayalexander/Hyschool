<?php
use App\Util\URL;
?>
<div id="slidetab" style="height:315px;">
  <div style="position:relative;">
    <div class="zoom_img" style="position:absolute;top:0;left:0;">
        <a data-placement="right" data-toggle="tooltip" href="<?=URL::SITE_ROOT;?>l_dashboard.php" title="Dashboard">
            <img src="<?=URL::SITE_ROOT;?>img/dashboard.png" />
        </a>
    </div>
    <div class="zoom_img" style="position:absolute;top:0;left:0;margin-top:48px;">
        <a data-placement="right" data-toggle="tooltip" href="<?=URL::SITE_ROOT;?>library/l_memberlist.php" title="Add Member">
            <img src="<?=URL::SITE_ROOT;?>img/member.png" />
        </a>
    </div>
    <div class="zoom_img" style="position:absolute;top:0;left:0;margin-top:100px;">
        <a data-placement="right" data-toggle="tooltip" href="<?=URL::SITE_ROOT;?>library/l_issuelist.php" title="Issue">
            <img src="<?=URL::SITE_ROOT;?>img/issue.png" />
        </a>
    </div>
    <div class="zoom_img" style="position:absolute;top:0;left:0;margin-top:145px;">
        <a data-placement="right" data-toggle="tooltip" href="<?=URL::SITE_ROOT;?>library/l_book_list.php" title="Book List">
            <img src="<?=URL::SITE_ROOT;?>img/books.png" />
        </a>
    </div>
    <div class="zoom_img" style="position:absolute;top:0;left:0;margin-top:195px;">
        <a data-placement="right" data-toggle="tooltip" href="<?=URL::SITE_ROOT;?>library/l_finelist.php" title="Fine">
            <img src="<?=URL::SITE_ROOT;?>img/fine.png" />
        </a>
    </div>
    <div class="zoom_img" style="position:absolute;top:0;left:0;margin-top:245px;">
        <a data-placement="right" data-toggle="tooltip" href="<?=URL::SITE_ROOT;?>logout.php" title="Logout">
            <img src="<?=URL::SITE_ROOT;?>img/logout.png" />
        </a>
    </div>
  </div>
</div>