<!DOCTYPE html>
<html class="js no-touch history boxshadow csstransforms3d csstransitions video svg webkit chrome win js sticky-header-enabled sticky-header-active">
<head>
		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>HySchool</title>	

		<meta name="keywords" content="School Management System,management software, school portal, 
        manage student's, parent and teacher records, system, Hyschool, real time access to student performance" />
		<meta name="description" content="This is a powerful, user friendly School Management System (SMS), that provides strong 
        interaction between parents and school managements through real time messaging and notifications. Realtime access to student
        performance assessment">
		<meta name="author" content="Hyschool.ng">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

		<!-- Favicon -->
        
		<link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
        <link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="img/favicon/apple-icon.png">
        <link rel="manifest" href="img/favicon/manifest.json">
        

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="vendor/font-awesome/css/fontawesome-all.min.css">
		<link rel="stylesheet" href="vendor/animate/animate.min.css">
		<link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="css/theme.css">
		<link rel="stylesheet" href="css/theme-elements.css">
		<link rel="stylesheet" href="css/theme-blog.css">
		<link rel="stylesheet" href="css/theme-display.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">
		
		<!-- Demo CSS -->
		<link rel="stylesheet" href="css/style.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="css/skin.css"> 

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="css/custom.css">

		<!-- Head Libs -->
        <style type="text/css">.gm-err-container{height:100%;width:100%;display:table;background-color:#e0e0e0;
		position:relative;left:0;top:0}.gm-err-content{border-radius:1px;padding-top:0;padding-left:10%;padding-right:10%;
		position:static;vertical-align:middle;display:table-cell}.gm-err-content a{color:#4285f4}.gm-err-icon{text-align:center}.gm-err-title{margin:5px;
		margin-bottom:20px;color:#616161;font-family:Roboto,Arial,sans-serif;text-align:center;font-size:24px}.gm-err-message{margin:5px;color:#757575;
		font-family:Roboto,Arial,sans-serif;text-align:center;font-size:12px}.gm-err-autocomplete{padding-left:20px;background-repeat:no-repeat;
		background-size:15px 15px}
		</style>
		<script src="vendor/modernizr/modernizr.min.js"></script>

	</head>
	<body data-spy="scroll" data-target=".wrapper-spy" data-offset="100">

		<div class="body">
			<header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAtElement': '#header', 'stickySetTop': '0', 'stickyChangeLogo': false}">
				<div class="header-body">
					<div class="header-container container-fluid px-0">
						<div class="header-row">
							<div class="header-column custom-divider-1">
								<div class="header-row">
									<div class="header-logo px-4">
										<a href="index.php">
											<img alt="Hyschool.ng logo" width="101" height="21" src="img/landing-page/logo.png">
										</a>
									</div>
								</div>
							</div>
							<div class="header-column justify-content-center w-100">
								<div class="header-row">
									<div class="header-nav justify-content-center">
										<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
											<nav class="wrapper-spy collapse">
												<ul class="nav nav-pills" id="mainNav">
													<li>
														<a class="nav-link active" href="#home" data-hash>
															Hyschool
														</a>
													</li>
													<li>
														<a class="nav-link" href="#who-we-are" data-hash data-hash-offset="32">
															Why Hyschool?
														</a>
													</li>
													<li>
														<a class="nav-link" href="#what-we-do" data-hash data-hash-offset="32">
															How it works
														</a>
													</li>
													<li>
														<a class="nav-link" href="#mobile_platform" data-hash data-hash-offset="32">
															Overview
														</a>
													</li>
													<li>
														<a class="nav-link" href="#contact-us" data-hash >
															Contact Us
														</a>
													</li>
												</ul>
                                                
											</nav>
										</div>
									</div>
								</div>
							</div>
							<div class="header-column custom-divider-1 _left justify-content-end">
								<div class="header-row px-4">
									<ul class="social-icons custom-social-icons-style-1 d-none d-md-flex">
										<li class="social-icons-facebook">
											<a title="Login" class="text-color-quaternary" href="http://hyschoo.ng/sms">
												Login
											</a>
										</li>
									</ul>
									<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
										<i class="fas fa-bars"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<div role="main" class="main">


				<div id="home" class="slider-container rev_slider_wrapper" style="height: 100vh;">
					<div id="revolutionSlider" class="slider manual rev_slider" data-version="5.4.7">
						<ul>
							<li data-transition="fade">
								<img src="img/landing-page/slides/slide-1.jpg"
									alt="hyschool.ng"
									data-bgposition="center center"
									data-bgfit="cover"
									data-bgrepeat="no-repeat"
									class="rev-slidebg">

								<h1 class="tp-caption text-color-light font-weight-bold"
									data-x="['left','left','left','left']" data-hoffset="['0','0','30','85']"
									data-y="center" data-voffset="-55"
									data-start="500"
									data-fontsize="80"
									data-transform_in="y:[-300%];opacity:0;s:500;" style="text-shadow: 0px 4px #31516A;">HYSCHOOL.NG</h1>

								<div class="tp-caption text-color-light"
									data-x="['left','left','left','left']" data-hoffset="['0','0','30','85']"
									data-y="['center','center','center','center']" data-voffset="['-10','-10','-10','0']"
									data-start="1500"
									data-fontsize="['18','18','18','25']"
									data-whitespace="nowrap"
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									data-mask_in="x:0px;y:0px;">INTRODUCING A POWERFUL SCHOOL MANAGEMENT SYSTEM</div>

								<div class="tp-caption"
									data-x="['left','left','left','left']" data-hoffset="['0','0','30','85']"
									data-y="['center','center','center','center']" data-voffset="['15','15','15','35']"
									data-start="2000"
									data-fontsize="['12','12','12','19']"
									data-transform_in="y:[100%];opacity:0;s:500;" style="color:#fc3;">A complete all in-one software...</div>

							</li>
							<li data-transition="fade">
								<img src="img/landing-page/slides/slide-3.jpg"
									alt="why hyschool"
									data-bgposition="center center"
									data-bgfit="cover"
									data-bgrepeat="no-repeat"
									class="rev-slidebg">
                                    <a href="#who-we-are">

								<h1 class="tp-caption text-color-light font-weight-bold"
									data-x="['left','left','left','left']" data-hoffset="['0','0','30','85']"
									data-y="center" data-voffset="-55"
									data-start="500"
									data-fontsize="80"
									data-transform_in="y:[-300%];opacity:0;s:500;">WHY HYSCHOOL?</h1><br /><br /><br />

								<div class="tp-caption text-color-light"
									data-x="['left','left','left','left']" data-hoffset="['0','0','30','85']"
									data-y="['center','center','center','center']" data-voffset="['10','10','10','0']"
									data-start="2000"
									data-fontsize="['18','18','18','25']"
									data-whitespace="nowrap"
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									data-mask_in="x:0px;y:0px;">Hyschool is a school management solution that puts effective school<br /> 
                                    management in the hands of school administrators and teachers and also allow for effective <br />
                                    collaboration with parents. </div>

								<div class="tp-caption"
									data-x="['left','left','left','left']" data-hoffset="['0','0','30','85']"
									data-y="['center','center','center','center']" data-voffset="['55','55','55','35']"
									data-start="2800"
									data-fontsize="['12','12','12','19']"
									data-transform_in="y:[100%];opacity:0;s:500;" style="color:#fc3;">and more ...</div>
                                    </a>

							</li>
                            <li data-transition="fade">
								<img src="img/landing-page/slides/slide-4.jpg"
									alt="hyschool how it works"
									data-bgposition="center center"
									data-bgfit="cover"
									data-bgrepeat="no-repeat"
									class="rev-slidebg">
                                    <a href="#what-we-do">

								<h1 class="tp-caption text-color-light font-weight-bold"
									data-x="['left','left','left','left']" data-hoffset="['0','0','30','85']"
									data-y="center" data-voffset="-55"
									data-start="500"
									data-fontsize="80"
									data-transform_in="y:[-300%];opacity:0;s:500;">HOW IT WORKS</h1>

								<div class="tp-caption text-color-light"
									data-x="['left','left','left','left']" data-hoffset="['0','0','30','85']"
									data-y="['center','center','center','center']" data-voffset="['-10','-10','-10','0']"
									data-start="1500"
									data-fontsize="['18','18','18','25']"
									data-whitespace="nowrap"
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									data-mask_in="x:0px;y:0px;">Teachers &hArr; 
                                    	<span style="padding:2em; border-radius:100%; border: solid 1px #fc3;"> Students/Pupils</span> 
                                      	&hArr; Parents
                                    </div>

								<div class="tp-caption"
									data-x="['left','left','left','left']" data-hoffset="['0','0','30','85']"
									data-y="['center','center','center','center']" data-voffset="['15','15','15','35']"
									data-start="2000"
									data-fontsize="['12','12','12','19']"
									data-transform_in="y:[100%];opacity:0;s:500;" style="color:#fc3;">The school admin oversee all 
                                    activities...</div>
                                    </a>

							</li>
                            <li data-transition="fade">
								<img src="img/landing-page/slides/slide-2.jpg"
									alt="hyschool mobile app"
									data-bgposition="center center"
									data-bgfit="cover"
									data-bgrepeat="no-repeat"
									class="rev-slidebg">
                                    <a href="#mobile_platform">

								<h1 class="tp-caption text-color-light font-weight-bold"
									data-x="['left','left','left','left']" data-hoffset="['0','0','30','85']"
									data-y="center" data-voffset="-55"
									data-start="500"
									data-fontsize="80"
									data-transform_in="y:[-300%];opacity:0;s:500;">MOBILE PLATFORM</h1>

								<div class="tp-caption text-color-light"
									data-x="['left','left','left','left']" data-hoffset="['0','0','30','85']"
									data-y="['center','center','center','center']" data-voffset="['-10','-10','-10','0']"
									data-start="1500"
									data-fontsize="['18','18','18','25']"
									data-whitespace="nowrap"
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									data-mask_in="x:0px;y:0px;">
                                    	Strong interaction between parents and schools through realtime messaging and notification.
                                    </div>

								<div class="tp-caption"
									data-x="['left','left','left','left']" data-hoffset="['0','0','30','85']"
									data-y="['center','center','center','center']" data-voffset="['15','15','15','35']"
									data-start="2000"
									data-fontsize="['12','12','12','19']"
									data-transform_in="y:[100%];opacity:0;s:500;" style="color:#fc3;">and more ...</div>
                                    </a>

							</li>
						</ul>
					</div>
				</div>

				<section id="who-we-are" class="section section-no-border background-color-light m-0">
					<div class="container">
						<div class="row text-center">
							<div class="col">
								<h2 style="color: #31516A;">WHY HYSCHOOL?</h2>
								<p class="custom-section-sub-title">HYSCHOOL  is a complete school management solution with but not limited
                                to the following features:</p>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								
                                    <style type="text/css">
										#whyHyschool li::before {
											content: "✓";
											padding: 10px;
											color: #fc3;
											font-weight: bold;											
										}
									</style>
                                    <ul id="whyHyschool" style="list-style:none; color: #31516A;">
                                    	<li>Strong Interaction between parents and schools through realtime messaging and notification. </li>
                                        <li>Realtime access to student performance assessment. </li>
                                        <li>Application can run on mobile phone. </li>
                                        <li>Electronic terminal report. </li>
                                        <li>Realtime daily attendance reports to parents. </li>
                                        <li>Manage students, parents and teachers records. </li>
                                        <li>Roboust assessment module, such as Computer Based Test (CBT). </li>
                                        <li>It's in the cloud</li>
                                        <li>It is user friendly and flexible</li>
                                        <li>IT'S <span style="color: #F00; text-decoration: underline;">FREE!</span></li>
                                    </ul>
								
                               </div>


							<div class="col-lg-6 custom-margin-3 custom-height text-center">
								<svg id="curved-line-1" class="d-none d-md-block" x="0px" y="0px" width="545px" height="305px" viewBox="0 0 545 305" enable-background="new 0 0 545 305" xml:space="preserve">
									<circle class="circle appear-animation" data-appear-animation="circle-anim" fill="none" stroke="#231F20" stroke-miterlimit="10" stroke-dasharray="2.0106,1.0053" cx="10.206" cy="9.91" r="8.167"/>
									<circle class="circle-dashed" fill="none" stroke="white" stroke-miterlimit="10" stroke-dasharray="3,3" cx="10.206" cy="9.91" r="8.167"/>
									<path class="path appear-animation" data-appear-animation="line-anim" data-appear-animation-delay="800" fill="none" stroke="#010101" stroke-miterlimit="10" stroke-dasharray="2.0024,2.0024" d="M11.469,21.046
										c3.191,19.81,32.779,130.736,292.756,87.863c280.979-46.337,240.717,145.948,212.215,185.401"/>
									<path class="path-dashed" fill="none" stroke="white" stroke-miterlimit="10" stroke-dasharray="3,3" d="M11.469,21.046
											c3.191,19.81,32.779,130.736,292.756,87.863c280.979-46.337,240.717,145.948,212.215,185.401"/>
								</svg>
								<img src="img/landing-page/why-hyschool-1.png" alt="why high school 1" class="custom-image-style-1 _left" data-appear-animation="zoomIn" data-appear-animation-delay="1300" />
								<img src="img/landing-page/why-hyschool-2.png" alt="why high school 2" class="custom-image-style-1 _middle" data-appear-animation="zoomIn" data-appear-animation-delay="1800" />
								<img src="img/landing-page/why-hyschool-3.png" alt="why high school 3" class="custom-image-style-1 _right" data-appear-animation="zoomIn" data-appear-animation-delay="2300" />
							</div>
						</div>
					</div>
				</section>

				<section class="section parallax section-parallax section-no-border custom-z-index m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="img/landing-page/parallax-1.jpg">
					<div class="container">
						<div class="row counters custom-counters">
							<div class="col-lg-3 col-sm-6">
								<div class="counter">
									<i class="fas fa-user-circle text-color-secondary"></i>
									<strong class="text-color-light" data-to="2000" data-append="+">0</strong>
	   								<label class="text-color-secondary">STUDENTS</label>
	   							</div>
							</div>
							<div class="col-lg-3 col-sm-6">
								<div class="counter">
									<i class="fas fa-user text-color-secondary"></i>
									<strong class="text-color-light" data-to="1598" data-append="+">0</strong>
	   								<label class="text-color-secondary">TEACHERS</label>
	   							</div>
							</div>
							<div class="col-lg-3 col-sm-6">
								<div class="counter">
									<i class="far fa-building text-color-secondary"></i>
									<strong class="text-color-light" data-to="1200" data-append="+">0</strong>
	   								<label class="text-color-secondary">SCHOOLS</label>
	   							</div>
							</div>
							<div class="col-lg-3 col-sm-6">
								<div class="counter">
									<i class="fas fa-users text-color-secondary"></i>
									<strong class="text-color-light" data-to="3000" data-append="+">0</strong>
	   								<label class="text-color-secondary">PARENTS</label>
	   							</div>
							</div>
						</div>
					</div>
				</section>

				<section id="what-we-do" class="section section-no-border background-color-light m-0 pb-0">
					<div class="container custom-pos-rel">
						<svg id="curved-line-2" class="d-none d-lg-block" x="0px" y="0px" width="132px" height="225px" viewBox="0 0 132 225" enable-background="new 0 0 132 225" xml:space="preserve">
							<circle class="circle" fill="none" stroke="#010101" stroke-miterlimit="10" stroke-dasharray="2,2" data-appear-animation="circle-anim" data-appear-animation-delay="1200" cx="120.888" cy="214.023" r="7.688"/>
							<circle class="circle-dashed" fill="none" stroke="white" stroke-miterlimit="10" stroke-dasharray="3,3" cx="120.888" cy="214.023" r="7.688"/>
							<path class="path" fill="none" stroke="#010101" stroke-miterlimit="10" stroke-dasharray="2,2" data-appear-animation="line-anim-2" d="M113.812,209.406c0,0-193-54.125-72.5-206.125"/>
							<path class="path-dashed" fill="none" stroke="white" stroke-miterlimit="10" stroke-dasharray="3,3" d="M113.812,209.406c0,0-193-54.125-72.5-206.125"/>
						</svg>
						<div class="row text-center">
							<div class="col">
								<h2  style="color: #31516A;">How it Works</h2>
								<p class="custom-section-sub-title">THE PRINCIPLE BEHIND HYSCHOOL</p>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="pr-4">
									<div class="feature-box feature-box-style-2 custom-feature-box-style-1">
		 								<div class="feature-box-icon" style="min-width: 3.4rem;">
		  									<i class="fas fa-object-group text-color-secondary"></i>
		  								</div>
		 								<div class="feature-box-info">
		  									<h4 class="mb-0">The school admin oversee all activities</h4>
		  									<p class="mb-4">The admin controls all activities within the school.</p>
		  								</div>
		 							</div>
		 							<div class="feature-box feature-box-style-2 custom-feature-box-style-1">
		 								<div class="feature-box-icon" style="min-width: 3.4rem;">
		  									<i class="fas fa-object-group text-color-secondary _size-1"></i>
		  								</div>
		 								<div class="feature-box-info">
		  									<h4 class="mb-0">Teachers and Parents give inputs.</h4>
		  									<p class="mb-4">
                                            	Teachers and Parents collaborate to give inputs on students and as well receives feedback
                                                simultaneously.
                                            </p>
		  								</div>
		 							</div>
		 							<a class="btn custom-btn-style-1 custom-margin-1 text-color-dark" href="#contact-us" data-hash>CONTACT US</a>
		 						</div>
							</div>
							<div class="col-lg-6">
								<img src="img/landing-page/how-it-works-2.jpg" alt="how it works 2" class="custom-image-style-2 _big" data-appear-animation="fadeIn" data-appear-animation-delay="1500" data-appear-animation-duration="100" />
								<img src="img/landing-page/how-it-works-1.png" alt="how it works 1" class="custom-image-style-2 _small" data-appear-animation="fadeInUp" data-appear-animation-delay="1900" data-appear-animation-duration="100" data-plugin-options="{'accY': 100}" />
							</div>
						</div>
					</div>
				</section>

				<section class="section section-no-border background-color-tertiary m-0">
					<div class="container">
						<div class="row text-center">
							<div class="col">
								<h2>Admin</h2>
							</div>
						</div>
						<div class="row justify-content-center">
							<div class="col">
								<div class="owl-carousel mb-0" data-plugin-options="{'items': 1, 'loop': false, 'dots': false, 'nav': false}">
									<div class="row justify-content-center">
										<div class="col-lg-8">
											<div class="testimonial testimonial-with-quotes testimonial-style-2 
                                            custom-testimonial-style-1 text-center mb-0 text-shadow"><br />
                                            	<h3 style="margin-top:1em">Teachers &hArr; 
                                    			<span style="padding:2em; border-radius:100%; border: solid 1px #fc3;"> Students/Pupils</span> 
                                      			&hArr; Parents</h3>
												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
                
                
				<section class="section section-primary section-no-border m-0" id="mobile_platform">
					<div class="container">
						<div class="row align-items-center">
							<div class="col-lg-6 mb-5 mb-lg-0">
								<img src="img/landing-page/mobile-platform.png" alt="hyschool mobile platform" class="img-fluid" />
							</div>
							<div class="col-lg-5">
								<h2 class="text-color-light">Mobile Platform</h2>
								<p class="custom-section-sub-title text-color-light">
                                	The solutions delivers effective school management to users on their mobile phones.
                                	The platform is intigrated with the following features:
                                </p>
                                <p class="text-color-light"><span> &raquo; </span>Attendance, Assessment, Reports and Admission management.
                                <br /><span> &raquo; </span>Student Record management.
                                <br /><span> &raquo; </span>Realtime Notifications for all users.
                                <br /><span> &raquo; </span>Workbook management and Online Assessment (CBT).</p>
                                <p class="text-color-light">All these at your finger tips.</p>
								<a href="#" class="btn custom-btn-style-1 _color-2 text-color-light mt-4">GET THE APP FROM GOOGLE PLAYSTORE NOW!</a>
							</div>
						</div>
					</div>
				</section>

				<!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
				<div id="contact-us" class="google-map m-0 custom-contact-pos">
                	<img src="img/landing-page/map-dark.png" alt="hyschool map" class="img-fluid" />
                </div>

				<section class="section section-no-border background-color-quaternary m-0 p-0">
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-lg-5 custom-contact-box custom-contact-pos background-color-quaternary">
								<h2 class="text-color-light">Contact Us</h2>
								<div class="feature-box feature-box-style-2 custom-feature-box-style-2 mb-4">
									<div class="feature-box-icon">
										<i class="icon-call-in icons text-color-secondary"></i>
									</div>
									<div class="feature-box-info">
										<h6 class="mb-0 text-2">Call us</h6>
										<a href="tel:+2347032059059" class="text-color-light text-decoration-none">+2347032059059, </a>
                                        <a href="tel:+2348077347969" class="text-color-light text-decoration-none">+2348077347969</a>
									</div>
								</div>
								<div class="feature-box feature-box-style-2 custom-feature-box-style-2 mb-4">
									<div class="feature-box-icon">
										<i class=" icon-envelope icons text-color-secondary"></i>
									</div>
									<div class="feature-box-info">
										<h6 class="mb-0 text-2">Email Us</h6>
										<a href="mailto:adeolaibigbemi@gmail.com" class="text-color-light text-decoration-none">
                                        	adeolaibigbemi@gmail.com
                                        </a><br />
                                        <a href="mailto:info@hyschool.ng" class="text-color-light text-decoration-none">
                                        	info@hyschool.ng
                                        </a>
									</div>
								</div>
								<h5 class="text-color-light">SEND A MESSAGE</h5>
								<form id="contactForm" class="custom-contact-form-style-1" method="POST">
									<div class="alert alert-success d-none mt-4" id="contactSuccess">
										<strong>Success!</strong> Your message has been sent to us.
									</div>

									<div class="alert alert-danger d-none mt-4" id="contactError">
										<strong>Error!</strong> There was an error sending your message.
										<span class="text-1 mt-2 d-block" id="mailErrorMessage"></span>
									</div>
									<div class="form-row _divider">
										<div class="form-group col-sm-6">
											<input type="text" value=""  maxlength="100" class="form-control" name="name" id="name" placeholder="YOUR NAME" required>
										</div>
										<div class="form-group col-sm-6">
											<input type="text" value="" maxlength="100" class="form-control" name="phone" id="phone" placeholder="PHONE" required>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col">
											<input type="email" value="" maxlength="100" class="form-control" name="email" id="email" placeholder="EMAIL ADDRESS" required>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col">
											<textarea maxlength="5000" rows="5" class="form-control custom-textarea-style" name="message" id="message" placeholder="COMMENT" required></textarea>
										</div>
									</div>
									<div class="form-row">
										<div class="col">
											<input type="submit" value="SUBMIT" class="btn btn-primary custom-btn-style-2 text-color-light custom-margin-2 float-right mt-2" name="submit">
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</section>

		</div>
		<footer id="footer" class="m-0 p-0">
			<div class="footer-copyright background-color-light">
				<div class="container">
					<div class="row">
						<div class="col-sm-9 mb-1">
							<p>© Copyright 2018. All Rights Reserved.</p>
						</div>
						<div class="col-sm-3 text-sm-right">
							<ul class="social-icons custom-social-icons-style-2">
								<li class="social-icons-facebook">
									<a href="http://www.facebook.com/" class="text-color-quaternary" target="_blank" title="Facebook">
										<i class="fab fa-facebook-f"></i>
									</a>
								</li>
								<li class="social-icons-twitter">
									<a href="http://www.twitter.com/" class="text-color-quaternary" target="_blank" title="Twitter">
										<i class="fab fa-twitter"></i>
									</a>
								</li>
								<li class="social-icons-linkedin">
									<a href="http://www.linkedin.com/" class="text-color-quaternary" target="_blank" title="Linkedin">
										<i class="fab fa-linkedin-in"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer>

		<!-- Vendor -->
		<script src="vendor/jquery/jquery.min.js"></script>
		<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
		<script src="vendor/popper/umd/popper.min.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="vendor/common/common.min.js"></script>
		<script src="vendor/jquery.validation/jquery.validation.min.js"></script>
		<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="vendor/isotope/jquery.isotope.min.js"></script>
		<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="vendor/vide/vide.min.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="js/theme.js"></script>
		
		<!-- Current Page Vendor and Views -->
		<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>		<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page Vendor and Views -->
		<script src="js/views/view.contact.js"></script>

		<!-- Demo -->
		<script src="js/demos/demo-one-page-agency.js"></script>
		
		<!-- Theme Custom -->
		<script src="js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="js/theme.init.js"></script>

	</body>

</html>
